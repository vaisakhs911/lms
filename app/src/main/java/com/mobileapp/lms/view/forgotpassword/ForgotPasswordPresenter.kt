package com.mobileapp.lms.view.forgotpassword

/**
 *  Module name:
 *  created: 5/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
class ForgotPasswordPresenter(
    private val forgotPasswordView: ForgotPasswordView,
    private val forgotPasswordInteractor: ForgotPasswordInteractor
) : ForgotPasswordInteractor.ForgotPasswordCallback {
    fun forgotPassword(email: String) {
        forgotPasswordInteractor.ForgotPassword(email, this)


    }

    override fun onSuccess() {
        forgotPasswordView.onSuccess()
    }

    override fun onError(error: String) {
        forgotPasswordView.onError(error)
    }

    override fun displayProgress() {
        forgotPasswordView.displayProgress()
    }

    override fun dismissProgress() {
        forgotPasswordView.dismissProgress()
    }

}


/*override fun onSuccess() {
                    forgotpasswordview.onSuccess()
                }

                override fun onError(error: String) {
                    forgotpasswordview.onError(error)
                }*/