package com.mobileapp.lms.view.leaverequest

/**
 *  Module name:
 *  created: 13/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
interface LeaveApproveView {
    fun onApproveSuccess()
    fun onApproveError(error: String)
    fun dismissProgress()
    fun displayProgress()
}