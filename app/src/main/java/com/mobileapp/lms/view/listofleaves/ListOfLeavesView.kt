package com.mobileapp.lms.view.listofleaves

import com.mobileapp.lms.Model.LeaveRequest.Base_class_Leave_Request

/**
 *  Module name:
 *  created: 30/1/20
 *  Author: AJITH TS
 *  Revisions:
 */
interface ListOfLeavesView {
    fun onError(error:String)
    fun displayProgress()
    fun dismissProgress()
    fun onLeaveListedSuccess(applyList: Base_class_Leave_Request)
    fun LeaveApprovedSuccess()
    fun LeaveApprovedError(error: String)
    fun LeaveRejectedSuccess()
    fun LeaveRejectedError(error: String)

}