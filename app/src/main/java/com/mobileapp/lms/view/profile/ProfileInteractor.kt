package com.mobileapp.lms.view.profile

import com.google.gson.Gson
import com.mobileapp.lms.Model.dummyMOdel.DummyMOdel
import com.mobileapp.lms.Presenter.Api.RestServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 *  Module name:
 *  created: 4/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
class ProfileInteractor {
    interface ProfileListCallback {
        fun onSuccess(profileList: DummyMOdel)
        fun onError(error: String)
        fun disPlayProgress()
        fun disMisProgress()


    }

    fun displayProfileData(id: Int, callback: ProfileListCallback) {
        callback.disPlayProgress()
        println("IDD:$id")
        RestServiceBuilder.apiService?.profile(id.toString())
            ?.enqueue(object : Callback<DummyMOdel> {
                override fun onFailure(call: Call<DummyMOdel>, t: Throwable) {

                    callback.onError(t.localizedMessage)
                    callback.disMisProgress()
                    println("Failure reason: ${t.localizedMessage}").toString()
                }

                override fun onResponse(
                    call: Call<DummyMOdel>,
                    response: Response<DummyMOdel>
                ) {
                    println("response:$response")
                    if (response.isSuccessful) {
                        callback.disMisProgress()
                        val successResponseMessage = response.body()
                        println("success profile Details${Gson().toJson(response.body())}")
                        println("Success:$successResponseMessage")
                        successResponseMessage?.let { callback.onSuccess(successResponseMessage) }
                    } else {
                        callback.disMisProgress()
                        var errorResponseMessage = response.errorBody().toString()
                        callback.onError(errorResponseMessage)
                        println("error:$errorResponseMessage")
                    }

                }

            })


    }
}