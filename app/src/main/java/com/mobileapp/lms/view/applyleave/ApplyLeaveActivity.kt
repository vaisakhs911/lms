package com.mobileapp.lms.view.applyleave

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.mobileapp.lms.Model.ApplyLeaveParam.ApplyleaveResponseModel
import com.mobileapp.lms.R
import kotlinx.android.synthetic.main.activity_apply_leave.*
import java.util.*


@Suppress("NAME_SHADOWING")
class ApplyLeaveActivity : AppCompatActivity(), ApplyLeaveView {

    private var mHour: Int = 0
    private var mMinute: Int = 0
    private var mApplyLeavePresenter: ApplyLeavePresenter? = null
    private var leaveType: Int? = null
    private var leavePattern: Int? = null
    var sharedPreferences: SharedPreferences? = null
    private val myPreferences = "LOGIN"
    var cal: Calendar
        get() = Calendar.getInstance()
        set(value) = TODO()
    var cal1: Calendar
        get() = Calendar.getInstance()
        set(value) = TODO()
    var selectDate1 = ""
    private var dialog: ProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {

        mHour = cal.get(Calendar.HOUR_OF_DAY)
        mMinute = cal.get(Calendar.MINUTE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_apply_leave)
        mApplyLeavePresenter = ApplyLeavePresenter(this, ApplyLeaveInteractor())
        sharedPreferences = getSharedPreferences(myPreferences, Context.MODE_PRIVATE)
        val id: Int = sharedPreferences?.getInt("id", 0)!!
        println("id${id}")
        initView()
        applyButtonClick(id)
        dialog = ProgressDialog(this)
        cancelButtonClick()


        sharedPreferences = getSharedPreferences(myPreferences, Context.MODE_PRIVATE)
        val userName: String = sharedPreferences?.getString("firstname", null)!!
        val employeeId: String = sharedPreferences?.getString("employeeid", null)!!
        var stringBuilder1 = StringBuilder()
        stringBuilder1.append(userName.toUpperCase())
        stringBuilder1.append("(")
        stringBuilder1.append(employeeId.toUpperCase())
        stringBuilder1.append(")")
        tv_name_emp_id.text = stringBuilder1


    }

    private fun applyButtonClick(id: Int) {
        rl_btn_apply.setOnClickListener {
            if (validationApplyLeave()) {
                sendDataToPresenterPost(id)
            }
        }
    }

    private fun cancelButtonClick() {
        rl_cancel_apply_leave.setOnClickListener {
            onBackPressed()
        }
    }


    private fun validationApplyLeave(): Boolean {


        when {
            spinner_leave_type.selectedItem.toString().trim() == "Select Type" -> {

                Toast.makeText(this, "Please Select the Type", Toast.LENGTH_SHORT).show()
                return false
            }
            spinner_leave_pattern.selectedItem.toString().trim() == "Day Type" -> {
                Toast.makeText(this, "Please Select the day Type", Toast.LENGTH_SHORT).show()
                return false
            }
            et_no_days.text.isNullOrEmpty() -> {
                et_no_days.error = "Please Fill the Number of days.."
                return false
            }
            tv_from.text.isNullOrEmpty() -> {
                tv_from.error = "Please select the date from which you want leave.."
                return false
            }
            tv_to.text.isNullOrEmpty() -> {
                tv_to.error = "Please select the date until which you want leave.."
                return false
            }
            et_reason.text.isNullOrEmpty() -> {
                et_reason.error = "Please Fill the reason.."
                return false
            }
            else -> {
                return true

            }


        }


    }


    private fun sendDataToPresenterPost(id: Int) {
        fetchSelectedLeaveType(spinner_leave_type.selectedItem.toString())
        fetchSelectedLeavePattern(spinner_leave_pattern.selectedItem.toString())
        println("leave type${leaveType}")



        mApplyLeavePresenter?.applyLeavePostPresenter(
            id, tv_name_emp_id.text.toString(), leavePattern!!,
            leaveType!!,

            et_no_days.text.toString(),
            tv_from.text.toString(),
            tv_to.text.toString(),
            et_reason.text.toString()
        )
        onBackPressed()
    }

    private fun fetchSelectedLeavePattern(selectedItem: String) {

        when (selectedItem) {
            "Select Type    " -> {
                leaveType = 0
            }
            "Casual Leave" -> {
                leaveType = 2
            }
            "Sick Leave" -> {
                leaveType = 1
            }
        }

        /*
         when (selectedItem) {
             "Day Type" -> {
                 leavePattern = 0
             }
             "Full Day" -> {
                 leavePattern = 2
             }
             "Half Day" -> {
                 leavePattern = 3
             }
         }*/
    }

    private fun fetchSelectedLeaveType(selectedItem: String) {


        when (selectedItem) {
            "Day Type" -> {
                leavePattern = 0
            }
            "Full Day" -> {
                leavePattern = 2
            }
            "Half Day" -> {
                leavePattern = 3
            }
        }


        /*  when (selectedItem) {
              "Select Type    " -> {
                  leaveType = 0
              }
              "Casual Leave" -> {
                  leaveType = 2
              }
              "Sick Leave" -> {
                  leaveType = 1
              }
          }*/

    }


    @SuppressLint("SetTextI18n")
    private fun initView() {
        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.leave_type,
            R.layout.simple_spinner_item_leavetype
        )

        spinner_leave_type.adapter = adapter
        adapter.setDropDownViewResource(R.layout.apply_spinner_dropdown_item)

        val leavePatternAdapter = ArrayAdapter.createFromResource(
            this,
            R.array.leavePatternArray,
            R.layout.simple_spinner_item_day_type
        )
        spinner_leave_pattern.adapter = leavePatternAdapter
        leavePatternAdapter.setDropDownViewResource(R.layout.apply2_spinner_dropdown_item)
        layout_back_apply_leave.setOnClickListener {

            onBackPressed()
        }

        getFromDate()
        getToDate()


    }

    @SuppressLint("SetTextI18n")
    private fun getToDate() {

        val dateSetListener1 =
            DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                val month = monthOfYear + 1
                cal1.set(Calendar.YEAR, year)
                cal1.set(Calendar.MONTH, monthOfYear)
                cal1.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                if (monthOfYear < 10) {
                    tv_to.text = "$dayOfMonth/0$month/$year"
                } else {
                    tv_to.text = " $dayOfMonth/$month/$year"
                }
                selectDate1 = tv_to.text as String
            }





        tv_to!!.setOnClickListener {
            DatePickerDialog(
                this@ApplyLeaveActivity,
                dateSetListener1,
                // set DatePickerDialog to point to today's date when it loads up
                cal1.get(Calendar.YEAR),
                cal1.get(Calendar.MONTH),
                cal1.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

    }

    private fun getFromDate() {
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                val month = monthOfYear + 1
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)

                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                if (monthOfYear < 10) {
                    tv_from.text = "$dayOfMonth/0$month/$year"
                } else {
                    tv_from.text = " $dayOfMonth/$month/$year"
                }
                println(tv_from)
                selectDate1 = tv_from.text as String
                Log.d("date", "$selectDate1")
            }

        tv_from!!.setOnClickListener {
            DatePickerDialog(
                this@ApplyLeaveActivity,
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

    }

    fun getValues(view: View) {

        Toast.makeText(
            this,
            "spinner" + spinner_leave_type.selectedItem.toString() + spinner_leave_pattern.selectedItem.toString(),
            Toast.LENGTH_LONG
        ).show()
    }


    override fun onError(error: String) {
        Toast.makeText(this, "Something went wrong!!!!!", Toast.LENGTH_SHORT).show()
    }

    override fun displayProgress() {
        /*dialog!!.setMessage("Loading. Please wait...")
        dialog!!.isIndeterminate = true
        dialog?.setCancelable(false)
        dialog!!.show()*/
    }

    override fun dismissProgress() {
        /*dialog?.dismiss()*/
    }

    override fun onLeaveAppliedSuccess(applyresponse: ApplyleaveResponseModel) {
        Toast.makeText(this, "Applied leave successfully", Toast.LENGTH_SHORT).show()
    }


}
