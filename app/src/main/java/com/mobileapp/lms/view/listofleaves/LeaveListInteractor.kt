package com.mobileapp.lms.view.listofleaves

import com.mobileapp.lms.Model.LeaveRequest.ApproveLeaveParams
import com.mobileapp.lms.Model.LeaveRequest.Base_class_Leave_Request
import com.mobileapp.lms.Model.LeaveRequest.RejectLeaveRequestParams
import com.mobileapp.lms.Presenter.Api.RestServiceBuilder
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 *  Module name:
 *  created: 30/1/20
 *  Author: AJITH TS
 *  Revisions:
 */
class LeaveListInteractor {


    interface ListOfLeaveCallback {
        fun onSuccess(applyList: Base_class_Leave_Request)
        fun onError(error: String)
        fun displayProgress()
        fun dismissProgress()
    }

    interface LeaveApproveCallback {

        fun onApproveSuccess()
        fun onApproveError(error: String)
    }

    interface LeaveRejectCallback {
        fun onRejectSuccess()
        fun onRejectError(error: String)

    }

    fun listOfLeavesGet(

        callback: ListOfLeaveCallback
    ) {
callback.displayProgress()
        /*  println("theid:$id")*/


        RestServiceBuilder.apiService?.listOfLeaves()
            ?.enqueue(object : Callback<Base_class_Leave_Request> {
                override fun onFailure(call: Call<Base_class_Leave_Request>, t: Throwable) {
                    callback.dismissProgress()
                    callback.onError(t.localizedMessage)
                    println("localizedmsg: ${t.localizedMessage}")

                }

                override fun onResponse(
                    call: Call<Base_class_Leave_Request>,
                    response: Response<Base_class_Leave_Request>

                ) {
                    println("response:$response")
                    if (response.isSuccessful) {
                        callback.dismissProgress()
                        val successResponseMessage = response.body()?.let { callback.onSuccess(it) }
                        println(message = "Success:$successResponseMessage")
                    } else {
                        callback.dismissProgress()
                        val errorResponseMessage = response.errorBody()?.string()
                        if (errorResponseMessage != null) {
                            callback.onError(errorResponseMessage)
                            println("error:$errorResponseMessage")
                        }

                    }
                }
            })

    }


    fun leaveApprovePost(
        id: Int, AppliedId: Int,
        callback1: LeaveApproveCallback
    ) {
        val leaveApproveParam = ApproveLeaveParams(1, AppliedId)
        RestServiceBuilder.apiService?.approveleave(id, leaveApproveParam)
            ?.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    callback1.onApproveError(t.localizedMessage)
                    println("Failure:${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {
                        val successResponseMessage = response.body()
                        println("success:$successResponseMessage")
                        callback1.onApproveSuccess()
                    } else {
                        val errorResponseMessage = response.errorBody()?.string()
                        println("error:$errorResponseMessage")
                        if (errorResponseMessage != null) {
                            callback1.onApproveError(errorResponseMessage)
                        }
                    }
                }


            })

    }


    fun leaveRejectPost(
        empId: Int,
        AppliedId: Int,
        reason: String,
        id: Int,
        callback2: LeaveRejectCallback
    ) {

        val rejectLeaveRequestParams = RejectLeaveRequestParams(id, 2, reason, empId)
        println("appliedid:$AppliedId")
        println("appliedid1:$empId")
        println("appliedid2:$id")


        RestServiceBuilder.apiService?.rejectleave(AppliedId, rejectLeaveRequestParams)
            ?.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    callback2.onRejectError(t.localizedMessage)
                    println("Failure:${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {
                        callback2.onRejectSuccess()
                        val successResponseMessage = response.body()
                        println("success:$successResponseMessage")
                    } else {
                        val errorResponseMessage = response.errorBody().toString()
                        println("error:$errorResponseMessage")
                        callback2.onRejectError(errorResponseMessage)

                    }
                }

            })

    }


}