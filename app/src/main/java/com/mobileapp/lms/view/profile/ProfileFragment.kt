package com.mobileapp.lms.view.profile

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.facebook.shimmer.ShimmerFrameLayout
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.*
import com.mobileapp.lms.Model.dummyMOdel.DummyMOdel
import com.mobileapp.lms.R
import com.mobileapp.lms.Presenter.Utils.TimeUtils
import com.mobileapp.lms.view.editProfile.EditProfileActivity
import kotlinx.android.synthetic.main.fragment_profile.*


/**
 * A simple [Fragment] subclass.
 */

class ProfileFragment : Fragment(), ProfileView {
    private var mprofilepresenter: ProfilePresenter? = null

    private var userType: String? = null
    private var txtProfile: TextView? = null
    private var txtProfileUType: TextView? = null
    private var txtProfileContactNo: TextView? = null
    private var txtProfileBloodGroup: TextView? = null
    private var txtProfileJobDescription: TextView? = null
    private var txtProfileAddress: TextView? = null
    private var sharedPreferences: SharedPreferences? = null
    private val myPreferences = "LOGIN"
    private var icEdit: ImageView? = null
    private var toolbarHeading: TextView? = null
    private var notificationCount: RelativeLayout? = null
    private var icNotification: ImageView? = null
    private var icSearch: ImageView? = null
    private var barChart: BarChart? = null
    private var pieChartCL: PieChart? = null
    private var pieChartSL: PieChart? = null
    private var emailProfile: TextView? = null
    private var joiningDateProfile: TextView? = null
    private var dateOfBirthProfile: TextView? = null
    private var rlMainLayout: RelativeLayout? = null

    private var mShimmerViewContainer: ShimmerFrameLayout? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for this fragment

        val view: View = inflater.inflate(R.layout.fragment_profile, container, false)

        initView(view)
        editButtonClick()
        mprofilepresenter = ProfilePresenter(this, ProfileInteractor())

        sharedPreferences = activity!!.getSharedPreferences(myPreferences, Context.MODE_PRIVATE)
        val id: Int = sharedPreferences?.getInt("id", 0)!!
        mprofilepresenter?.displayProfileData(id)

        return view


    }


    private fun inputBarChartData() {
        val noOfEmp: ArrayList<BarEntry> = ArrayList()
        noOfEmp.add(BarEntry(945f, 0f))
        noOfEmp.add(BarEntry(1040f, 1f))
        noOfEmp.add(BarEntry(1133f, 2f))
        noOfEmp.add(BarEntry(1240f, 3f))
        noOfEmp.add(BarEntry(1369f, 4f))
        noOfEmp.add(BarEntry(1487f, 5f))
        noOfEmp.add(BarEntry(1501f, 6f))
        noOfEmp.add(BarEntry(1645f, 7f))
        noOfEmp.add(BarEntry(1578f, 8f))
        noOfEmp.add(BarEntry(1695f, 9f))
        val barDataSet = BarDataSet(noOfEmp, "No Of Employee")
        val year = ArrayList<Any>()
        year.add("2008")
        year.add("2009")
        year.add("2010")
        year.add("2011")
        year.add("2012")
        year.add("2013")
        year.add("2014")
        year.add("2015")
        year.add("2016")
        year.add("2017")
        barChart?.animateY(5000)
        val data = BarData(barDataSet)
        data.barWidth = 20f
        barDataSet.color = resources.getColor(R.color.yellow)
        barChart?.data = data
    }

    private fun setToolbarVisibility() {
        toolbarHeading?.text = "My Profile"
        notificationCount?.visibility = View.GONE
        icNotification?.visibility = View.GONE
        icEdit?.visibility = View.VISIBLE
        icSearch?.visibility = View.GONE
    }

    private fun initView(view: View) {
        rlMainLayout = view.findViewById(R.id.rl_main_layout)
        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container)
        barChart = view.findViewById(R.id.barchart)
        pieChartCL = view.findViewById(R.id.piechart_cl)
        pieChartSL = view.findViewById(R.id.piechart_sl)
        icEdit = activity?.findViewById(R.id.edit_profile_icon)
        toolbarHeading = activity?.findViewById(R.id.tv_heading_hme_screen)
        notificationCount = activity?.findViewById(R.id.rl_count_of_notificatns_feeds)
        icNotification = activity?.findViewById(R.id.img_notifications_active_feeds)
        icSearch = activity?.findViewById(R.id.img_searchbtn_activityfeeds)
        txtProfile = view.findViewById(R.id.tv_name_profile)
        txtProfileUType = view.findViewById(R.id.tv_user_type_profile)
        txtProfileContactNo = view.findViewById(R.id.tv_contact_no_profile)
        txtProfileBloodGroup = view.findViewById(R.id.tv_blood_group_profile)
        txtProfileJobDescription = view.findViewById(R.id.tv_designation_profile)
        txtProfileAddress = view.findViewById(R.id.tv_address_profile)
        emailProfile = view.findViewById(R.id.tv_email_profile)
        joiningDateProfile = view.findViewById(R.id.tv_joining_date_profile)
        dateOfBirthProfile = view.findViewById(R.id.tv_dob_profile)




        setToolbarVisibility()
        inputBarChartData()
        inputPieChartData()
    }


    override fun onResume() {
        super.onResume()
        mShimmerViewContainer!!.startShimmerAnimation()
    }

    override fun onPause() {
        mShimmerViewContainer!!.stopShimmerAnimation()
        super.onPause()
    }

    private fun inputPieChartData() {
        val noOfLeaves: ArrayList<PieEntry> = ArrayList()
        noOfLeaves.add(PieEntry(945f, 30f))
        noOfLeaves.add(PieEntry(1040f, 80f))
        noOfLeaves.add(PieEntry(1133f, 60f))
        noOfLeaves.add(PieEntry(1240f, 40f))
        noOfLeaves.add(PieEntry(1369f, 49f))
        noOfLeaves.add(PieEntry(1487f, 58f))
        noOfLeaves.add(PieEntry(1501f, 37f))
        noOfLeaves.add(PieEntry(1645f, 6f))
        noOfLeaves.add(PieEntry(1578f, 8f))
        noOfLeaves.add(PieEntry(1695f, 9f))
        val year1 = ArrayList<Any>()
        year1.add("2008")
        year1.add("2009")
        year1.add("2010")
        year1.add("2011")
        year1.add("2012")
        year1.add("2013")
        year1.add("2014")
        year1.add("2015")
        year1.add("2016")
        year1.add("2017")
        val pieDataSet = PieDataSet(noOfLeaves, "Remaining Leaves")
        val pieData = PieData(pieDataSet)
        pieChartCL?.data = pieData
        pieChartCL?.animateXY(5000, 5000)
        pieChartSL?.data = pieData
        pieChartSL?.animateXY(5000, 5000)
    }

    private fun editButtonClick() {
        icEdit?.setOnClickListener {
            val intent = Intent(context, EditProfileActivity::class.java)
            startActivity(intent)

        }
    }

    override fun dismissProgress() {
//        dialog?.dismiss()
        rlMainLayout?.visibility = View.VISIBLE
        mShimmerViewContainer?.stopShimmerAnimation()
        mShimmerViewContainer?.visibility = View.GONE
    }

    override fun displayProgress() {
        rlMainLayout?.visibility = View.GONE
        mShimmerViewContainer?.startShimmerAnimation()
        mShimmerViewContainer?.visibility = View.VISIBLE

        /*dialog!!.setMessage("Loading. Please wait...")
        dialog!!.isIndeterminate = true
        dialog?.setCancelable(false)
        dialog!!.show()
*/
    }


    override fun onError(error: String) {
        Toast.makeText(context, "something went wrong", Toast.LENGTH_SHORT).show()

    }

    @SuppressLint("DefaultLocale")
    override fun onSuccess(profileList: DummyMOdel) {
        val image = profileList.result!![0].photo
        val baseurl: String = "https://cubets3dev.s3.us-west-1.amazonaws.com/"

        val dateOfBirth = profileList.result!![0].dOB?.let {
            TimeUtils.convertSrverformattoSimple2(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
                it
            )
        }

        println("IMAGE:$image")
        if (img_profile_pic != null) {
            Glide
                .with(context!!)
                .load("https://cubets3dev.s3.us-west-1.amazonaws.com/" + image)
                .into(img_profile_pic)
        }
        //println("path:$stringBuilder3")
        txtProfile?.text = userNameProfile(profileList)
        println("name:${txtProfile?.text}")
        emailProfile?.text = profileList.result!![0].email
        joiningDateProfile?.text = joiningDate(profileList)
        dateOfBirthProfile?.text = dateOfBirth
        val userType = profileList.result!![0].userType?.toInt()
        fetchUserType(userType)
        txtProfileUType?.text = this.userType
        txtProfileContactNo?.text = profileList.result!![0].phone
        txtProfileBloodGroup?.text = profileList.result!![0].bloodGroup
        println("Blood:$")
        txtProfileAddress?.text = setAddress(profileList)
        txtProfileJobDescription?.text = jobDescription(profileList)
        println("profiledata:$profileList")

    }

    private fun jobDescription(profileList: DummyMOdel): StringBuilder {
        val stringBuilder5 = StringBuilder()
        stringBuilder5.append(profileList.result!![0].designation?.toUpperCase())
        stringBuilder5.append("-")
        stringBuilder5.append(profileList.result!![0].departmentname?.toUpperCase())
        return stringBuilder5
    }

    private fun joiningDate(profileList: DummyMOdel): StringBuilder {
        val stringBuilder2 = StringBuilder()
        stringBuilder2.append("Joined on: ")
        stringBuilder2.append(profileList.result!![0].dateOfJoining?.let {
            TimeUtils.convertSrverformattoSimple1(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
                it
            )
        })
        return stringBuilder2
    }

    private fun userNameProfile(profilelist: DummyMOdel): StringBuilder {
        val stringBuilder1 = StringBuilder()
        stringBuilder1.append(profilelist.result!![0].firstname?.toUpperCase())
        stringBuilder1.append("(")
        stringBuilder1.append(profilelist.result!![0].employeeId?.toUpperCase())
        stringBuilder1.append(")")
        return stringBuilder1
    }

    private fun setAddress(profilelist: DummyMOdel): StringBuilder {
        val stringBuilder = StringBuilder()
        stringBuilder.append(profilelist.result!![0].address)
        stringBuilder.append(System.lineSeparator())
        stringBuilder.append(profilelist.result!![0].address2)
        stringBuilder.append(System.lineSeparator())
        stringBuilder.append(profilelist.result!![0].country)
        stringBuilder.append(System.lineSeparator())
        stringBuilder.append(profilelist.result!![0].pincode)
        return stringBuilder
    }


    private fun fetchUserType(usertype: Int?) {
        when (usertype) {
            0 ->
                userType = "Employee"
            1 -> userType = "Admin"
        }

    }


}
