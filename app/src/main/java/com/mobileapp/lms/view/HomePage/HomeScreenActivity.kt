package com.mobileapp.lms.view.HomePage

import ExpandableMenuListAdapter
import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.navigation.NavigationView
import com.mobileapp.lms.Model.dummyMOdel.DummyMOdel
import com.mobileapp.lms.Model.side_menu_model.SideMenuModel
import com.mobileapp.lms.R
import com.mobileapp.lms.Presenter.Utils.CommonUtils
import com.mobileapp.lms.view.LeaveManagementFragment
import com.mobileapp.lms.view.Widget.FullDrawerLayout
import com.mobileapp.lms.view.activityfeeds.ActivityFeedsFragment
import com.mobileapp.lms.view.login.LMSLogin
import com.mobileapp.lms.view.profile.ProfileFragment
import kotlinx.android.synthetic.main.home_screen.*


class HomeScreenActivity : AppCompatActivity(), LogoutView {
    private val idConstant = "id"
    private val firstName = "firstname"
    private val userType = "usertype"
    val employeeId = "employeeid"
    private val accessToken = "accesstoken"
    private val loginSuccess = "loginSuccesskey"
    private lateinit var homeFragment: ActivityFeedsFragment
    private var mMenuAdapter: ExpandableMenuListAdapter? = null
    private var expandableList: ExpandableListView? = null
    private var listDataHeader: ArrayList<String>? = null
    private var listDataChild: HashMap<String, List<SideMenuModel>>? = null
    private lateinit var drawerLayout: FullDrawerLayout
    private lateinit var toggle: ActionBarDrawerToggle
    private var mLogoutPresenter: LogoutPresenter? = null
    private var sharedPreferences: SharedPreferences? = null
    private val myPreferences = "LOGIN"
    private var navigationView: NavigationView? = null
    private var header: View? = null
    private var headerUserName: TextView? = null
    private var headerDesignation: TextView? = null
    private var headerImage: ImageView? = null
    private var dialog: ProgressDialog? = null

    @SuppressLint("RestrictedApi")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialog = ProgressDialog(this)
        mLogoutPresenter = LogoutPresenter(this, LogoutInteractor())
        setContentView(R.layout.home_screen)

        initView()
        prepareNavListData()
        defaultFragmentView()
        expandableChildClick()
        expandableParentClick()
        bottomNavigationClick()
        initViewHeader()
        headerImageClick()

    }

    private fun headerImageClick() {

        headerImage?.setOnClickListener {
            var fragment: Fragment? = null
            fragment = ProfileFragment()


            if (fragment != null) {
                val ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.content_frame, fragment)
                ft.commit()
            }
            val drawer = findViewById<View>(R.id.drawer) as DrawerLayout
            drawer.closeDrawer(GravityCompat.START)
            false
        }

    }

    private fun initViewHeader() {

        navigationView = findViewById(R.id.nav_view)
        header = navigationView!!.getHeaderView(0)
        headerUserName = header!!.findViewById(R.id.txt_user_name)
        headerDesignation = header!!.findViewById(R.id.text_header_designation)
        headerImage = header!!.findViewById(R.id.header_user)



    }

    private fun bottomNavigationClick() {
        navigation.setOnNavigationItemSelectedListener { item ->

            displaySelectedScreen(item.itemId)
            val menu: Menu = navigation.menu
            val item1 = menu.getItem(2)
            item.isChecked = item1.itemId == item.itemId
            false

        }


    }


    private fun expandableParentClick() {
        expandableList?.setOnGroupClickListener { parent, v, groupPosition, id ->
            var fragment: Fragment? = null
            when (groupPosition) {
                0 -> {
                    fragment = ActivityFeedsFragment()

                }
                2 -> {
                    fragment = LeaveManagementFragment()
                }
                6 -> {
                    fragment = ProfileFragment()
                }
                7 -> {
                    logout()

                }
            }
            if (fragment != null) {
                val ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.content_frame, fragment)
                ft.commit()
            }
            val drawer = findViewById<View>(R.id.drawer) as DrawerLayout
            drawer.closeDrawer(GravityCompat.START)
            false
        }
    }

    private fun expandableChildClick() {
        expandableList?.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->

            false
        }
    }






    private fun defaultFragmentView() {
        mMenuAdapter = ExpandableMenuListAdapter(
            this,
            listDataHeader,
            listDataChild,
            expandableList
        )
        homeFragment = ActivityFeedsFragment()
        val fragmentManager = supportFragmentManager
        fragmentManager.beginTransaction().replace(R.id.content_frame, homeFragment).commit()
        expandableList?.setAdapter(mMenuAdapter)
    }

    private fun initView() {
        val toggleHome = findViewById<ImageView>(R.id.toggle)
        val actionBar = supportActionBar
        expandableList = findViewById(R.id.expandable_list_vew)
        /*email=activity.findViewById(R.id.et_email_address_lgn)*/
        drawerLayout = findViewById(R.id.drawer)
        toggle = ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        actionBar?.setDisplayHomeAsUpEnabled(false)
        toggleButtonClick(toggleHome)


    }

    private fun toggleButtonClick(toggleHome: ImageView) {
        toggleHome.setOnClickListener {
            sharedPreferences = this.getSharedPreferences(myPreferences, Context.MODE_PRIVATE)
            val id: Int = sharedPreferences?.getInt("id", 0)!!
            mLogoutPresenter!!.displayData(id)
            drawerLayout.openDrawer(GravityCompat.START, true)
            CommonUtils.hideKeyBoard(this)

        }

    }


    @SuppressLint("InflateParams")
    private fun logout() {
        sharedPreferences = getSharedPreferences(myPreferences, Context.MODE_PRIVATE)
        val userId: Int = sharedPreferences?.getInt("id", 0)!!
        val popupView: View =
            LayoutInflater.from(this).inflate(R.layout.popup_logout, null)
        val popupWindow = PopupWindow(
            popupView,
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        val logoutOkButton = popupView.findViewById<RelativeLayout>(R.id.rlayout_logout_ok)
        val logoutCancelButton = popupView.findViewById<RelativeLayout>(R.id.rlayout_logout_cancel)

        logoutOkButtonClick(logoutOkButton, userId, popupWindow)
        logoutCancelButtonClick(logoutCancelButton, popupWindow)

        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0)
    }

    private fun logoutCancelButtonClick(
        logoutCancelButton: RelativeLayout,
        popupWindow: PopupWindow
    ) {
        logoutCancelButton.setOnClickListener {
            popupWindow.dismiss()
        }

    }


    private fun logoutOkButtonClick(
        logoutOkButton: RelativeLayout,
        userId: Int,
        popupWindow: PopupWindow
    ) {
        logoutOkButton.setOnClickListener {
            mLogoutPresenter?.logout(userId)
            sharedPreferences = getSharedPreferences(myPreferences, Context.MODE_PRIVATE)
            val editor = sharedPreferences?.edit()
            editor?.putInt(idConstant, 0)
            editor?.putString(firstName, "")
            editor?.putInt(userType, 0)
            editor?.putString(employeeId, "")
            editor?.putString(accessToken, "")
            editor?.putBoolean(loginSuccess, false)
            editor?.apply()
            editor?.clear()
            println("logout is clicked")
            popupWindow.dismiss()
            val homeIntent = Intent(Intent.ACTION_MAIN)
            homeIntent.addCategory(Intent.CATEGORY_HOME)
            homeIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(homeIntent)
            val intent = Intent(this, LMSLogin::class.java)
            startActivity(intent)


        }
    }


    private fun prepareNavListData() {

        listDataHeader = ArrayList<String>()
        listDataChild = HashMap<String, List<SideMenuModel>>()
        // Adding data header
        addingDataHeader()
        settingSideMenuSettings()
        settingSideMenuInterview()

    }

    private fun settingSideMenuInterview() {
        val sideMenuModel8 = SideMenuModel()
        sideMenuModel8.title = "Add Candidate"
        sideMenuModel8.image = R.drawable.permission

        val sideMenuModel9 = SideMenuModel()
        sideMenuModel9.title = "Interview Schedule"
        sideMenuModel9.image = R.drawable.permission

        val sideMenuModel10 = SideMenuModel()
        sideMenuModel10.title = "List of Candidates"
        sideMenuModel10.image = R.drawable.permission

        val heading2: MutableList<SideMenuModel> = ArrayList()
        heading2.add(sideMenuModel8)
        heading2.add(sideMenuModel9)
        heading2.add(sideMenuModel10)
        listDataChild!![listDataHeader!![4]] = heading2

    }

    private fun settingSideMenuSettings() {

        val sideMenuModel = SideMenuModel()
        sideMenuModel.title = "permission"
        sideMenuModel.image = R.drawable.permission

        val sideMenuModel1 = SideMenuModel()
        sideMenuModel1.title = "modules"
        sideMenuModel1.image = R.drawable.permission

        val sideMenuModel2 = SideMenuModel()
        sideMenuModel2.title = "Employee management"
        sideMenuModel2.image = R.drawable.permission


        val sideMenuModel3 = SideMenuModel()
        sideMenuModel3.title = "Department"
        sideMenuModel3.image = R.drawable.permission


        val sideMenuModel4 = SideMenuModel()
        sideMenuModel4.title = "Designation"
        sideMenuModel4.image = R.drawable.permission

        val sideMenuModel5 = SideMenuModel()
        sideMenuModel5.title = "Holidays"
        sideMenuModel5.image = R.drawable.permission

        val sideMenuModel6 = SideMenuModel()
        sideMenuModel6.title = "Employee List"
        sideMenuModel6.image = R.drawable.permission
        val sideMenuModel7 = SideMenuModel()
        sideMenuModel7.title = "Team members"
        sideMenuModel7.image = R.drawable.permission
        val heading1: MutableList<SideMenuModel> = ArrayList()

        heading1.add(sideMenuModel)
        heading1.add(sideMenuModel1)
        heading1.add(sideMenuModel2)
        heading1.add(sideMenuModel3)
        heading1.add(sideMenuModel4)
        heading1.add(sideMenuModel5)
        heading1.add(sideMenuModel6)
        heading1.add(sideMenuModel7)

        listDataChild!!.put(listDataHeader!!.get(3), heading1)
    }

    private fun addingDataHeader() {
        listDataHeader!!.add("Activity feeds")
        listDataHeader!!.add("My Post")
        listDataHeader!!.add("Leave Manage")
        listDataHeader!!.add("settings")
        listDataHeader!!.add("Interview details")
        listDataHeader!!.add("feedback")
        listDataHeader!!.add("profile")
        listDataHeader!!.add("logout")
    }

    private fun displaySelectedScreen(itemId: Int) {

        var bottomFragment: Fragment? = null

        when (itemId) {
            R.id.activity_feeds -> {
                bottomFragment =
                    ActivityFeedsFragment()

            }
            R.id.profile -> {
                bottomFragment = ProfileFragment()

            }
            R.id.leave_management -> {
                bottomFragment = LeaveManagementFragment()

            }

        }
        if (bottomFragment != null) {
            val ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.content_frame, bottomFragment)
            ft.commit()
        }
        val drawer = findViewById<View>(R.id.drawer) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
    }

    override fun onError(error: String) {
        Toast.makeText(this, "Something went wrong.....", Toast.LENGTH_SHORT).show()
    }

    override fun displayProgress() {
        dialog!!.setMessage("Loading. Please wait...")
        dialog!!.isIndeterminate = true
        dialog?.setCancelable(false)
        dialog!!.show()
    }

    override fun dismissProgress() {
        dialog?.dismiss()
    }

    override fun onSuccess() {
        Toast.makeText(this, "Logout Successfully...", Toast.LENGTH_SHORT).show()
    }

    override fun onSuccess(profileList: DummyMOdel) {
        val image = profileList.result!![0].photo

        headerUserName?.text = profileList.result!![0].firstname?.toUpperCase()
        headerDesignation?.text = profileList.result!![0].designation?.toUpperCase()
        val into = Glide
            .with(this)
            .load("https://cubets3dev.s3.us-west-1.amazonaws.com/" + image)
            .apply(RequestOptions.circleCropTransform())
            .apply(RequestOptions().override(90, 90))
            .into(headerImage!!)


        /*headerDesignation=header!!.findViewById(R.id.text_header_designation)
        headerImage=*/

    }


    override fun onDisplayError(error: String) {
    }


}




