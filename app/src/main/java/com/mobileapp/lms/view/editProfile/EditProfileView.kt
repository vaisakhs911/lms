package com.mobileapp.lms.view.editProfile

import com.mobileapp.lms.Model.dummyMOdel.DummyMOdel

/**
 *  Module name:
 *  created: 5/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
interface EditProfileView {
    fun dismissProgress()
    fun displayProgress()
    fun onSuccess()
    fun onError(error: Any?)
    fun uploadSuccess(path: String)
    fun onUploadError(error: String)
    fun editPreviewSuccess(profileList: DummyMOdel)
    fun editPreviewError(error: String)
    fun displayPreviewProgress()
    fun dismissPreviewProgress()
}