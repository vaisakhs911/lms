package com.mobileapp.lms.view.applyleave

import com.mobileapp.lms.Model.ApplyLeaveParam.ApplyleaveResponseModel
import com.mobileapp.lms.Model.dummyMOdel.DummyMOdel

/**
 *  Module name:
 *  created: 28/1/20
 *  Author: AJITH TS
 *  Revisions:
 */
interface ApplyLeaveView {
    fun onError(error:String)
    fun displayProgress()
    fun dismissProgress()
    fun onLeaveAppliedSuccess(applyresponse: ApplyleaveResponseModel)

}