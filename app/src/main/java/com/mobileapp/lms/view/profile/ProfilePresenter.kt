package com.mobileapp.lms.view.profile

import com.mobileapp.lms.Model.dummyMOdel.DummyMOdel

/**
 *  Module name:
 *  created: 4/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
class ProfilePresenter(
    private val profileView: ProfileView,
    private val profileInteractor: ProfileInteractor


) : ProfileInteractor.ProfileListCallback {
    fun displayProfileData(id: Int) {

        profileInteractor.displayProfileData(id, this)

    }


    override fun onSuccess(profileList: DummyMOdel) {
        profileView.onSuccess(profileList)
    }


    override fun onError(error: String) {
        profileView.onError(error)
    }

    override fun disPlayProgress() {
        profileView.displayProgress()
    }

    override fun disMisProgress() {
        profileView.dismissProgress()
    }


}