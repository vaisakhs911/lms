package com.mobileapp.lms.view.leaverequest

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.shimmer.ShimmerFrameLayout
import com.mobileapp.lms.Model.LeaveRequest.Base_class_Leave_Request
import com.mobileapp.lms.Presenter.Adapter.LeaveRequestAdapter
import com.mobileapp.lms.R


class LeaveRequest : AppCompatActivity(), LeaveRequestView, LeaveApproveView, LeaveRejectView {
    private var mShimmerViewContainer: ShimmerFrameLayout? = null
    private var rlMainLayout: LinearLayout? = null
    private var recyclerView: RecyclerView? = null
    lateinit var context: Context
    private var mLeaveRequestPresenter: LeaveRequestPresenter? = null
    private var backButton: RelativeLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leave_request)
        recyclerView = findViewById(R.id.rv_leave_request)
        recyclerView!!.layoutManager = LinearLayoutManager(this)
        mLeaveRequestPresenter = LeaveRequestPresenter(this, LeaveRequesrInteractor(), this, this)

        mLeaveRequestPresenter?.leaveRequests()
        initView()


    }

    private fun initView() {
        backButton = findViewById(R.id.back_leave_request)
        rlMainLayout = findViewById(R.id.main_layout_leave_requests)
        mShimmerViewContainer =findViewById(R.id.shimmer_view_container_leave_request)
        backButtonClick(backButton!!)
    }

    private fun backButtonClick(backButton: RelativeLayout) {
        backButton.setOnClickListener {

            onBackPressed()
        }
    }

    override fun onError(error: String) {
        Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show()
    }


    override fun onSuccess(applyList: Base_class_Leave_Request) {


        if(applyList!=null && applyList.result!=null) {
            val adapter = LeaveRequestAdapter(applyList.result, this)
            recyclerView?.adapter = adapter
            adapter.notifyDataSetChanged()
        }else{
            recyclerView!!.visibility= View.GONE
        }

        println("Model:$applyList")
    }


    override fun dismissProgress() {
        rlMainLayout?.visibility=View.VISIBLE
        mShimmerViewContainer?.stopShimmerAnimation()
        mShimmerViewContainer?.visibility = View.GONE

    }

    override fun displayProgress() {
        rlMainLayout?.visibility=View.GONE
        mShimmerViewContainer?.startShimmerAnimation()
        mShimmerViewContainer?.visibility = View.VISIBLE

    }

    override fun onApproveSuccess() {
        Toast.makeText(this, "Leave approved successfully", Toast.LENGTH_SHORT).show()


    }
    override fun onResume() {
        super.onResume()
        mShimmerViewContainer!!.startShimmerAnimation()                  //to avoid background Tasks
    }

    override fun onPause() {
        mShimmerViewContainer!!.stopShimmerAnimation()
        super.onPause()
    }


    override fun onApproveError(error: String) {
        Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show()

    }

    override fun onRejectSuccess() {
        Toast.makeText(this, "Leave Rejected successfully", Toast.LENGTH_SHORT).show()

    }

    override fun onRejectError(error: String) {
        Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show()
    }

}
