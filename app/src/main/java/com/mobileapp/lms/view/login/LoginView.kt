package com.mobileapp.lms.view.login

import com.example.LoginResponseModel


/**
 *  Module name:
 *  created: 18/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
interface LoginView {
    fun onError(error:String)
    fun displayProgress()
    fun dismissProgress()
    fun onSuccess(loginLog: LoginResponseModel)
}