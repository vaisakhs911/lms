package com.mobileapp.lms.view.HomePage

import com.mobileapp.lms.Model.dummyMOdel.DummyMOdel

/**
 *  Module name:
 *  created: 24/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
interface LogoutView { fun onError(error:String)
    fun displayProgress()
    fun dismissProgress()
    fun onSuccess()
    fun onSuccess(profileList: DummyMOdel)
    fun onDisplayError(error: String)

}