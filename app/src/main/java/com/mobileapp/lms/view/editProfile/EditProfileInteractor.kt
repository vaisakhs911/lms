package com.mobileapp.lms.view.editProfile

import android.content.Context
import android.util.Log
import androidx.annotation.NonNull
import com.google.gson.Gson
import com.mobileapp.lms.Model.dummyMOdel.DummyMOdel
import com.mobileapp.lms.Model.editProfile.EditProfileParamPost
import com.mobileapp.lms.Presenter.Api.RestServiceBuilder
import com.mobileapp.lms.Presenter.Utils.TimeUtils
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File


/**
 *  Module name:
 *  created: 5/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
class EditProfileInteractor(private var mContext: Context) {


    interface EditProfileCallback {
        fun onSuccess()
        fun onError(error: String)
        fun displayProgress()
        fun dismissProgress()


    }

    interface UploadPicCallback {
        fun uploadSuccess(path: String)
        fun onUploadError(error: String)
        fun displayProgress()
        fun dismissProgress()


    }

    interface PreviewDetailsCallback {
        fun editPreviewSuccess(profileList: DummyMOdel)
        fun editPreviewError(error: String)
        fun displayPreviewProgress()
        fun dismissPreviewProgress()


    }

    fun editProfilePatch(
        id: Int,
        FirstName: String,
        pinCode: String,
        dob: String,
        address1: String,
        address2: String,
        country: String,
        phone: String,
        bloodGroup: String,
        uploadedImagePath: String?,
        callback: EditProfileCallback
    ) {

        callback.displayProgress()
        val dateOfBirth = TimeUtils.convertTimeToServerFormat("dd/MM/yyyy", dob)
        println("FromDate :$dateOfBirth")
        val editProfileParamPost =
            EditProfileParamPost(
                FirstName, pinCode.toInt(), address1, address2,
                country, phone
                , dateOfBirth, bloodGroup, uploadedImagePath
            )
        println("interactor:$address2")
        println("Blood group:$bloodGroup")
        println("Image path : $uploadedImagePath")
        RestServiceBuilder.apiService?.editprofile(id.toString(), editProfileParamPost)
            ?.enqueue(object : Callback<ResponseBody> {

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    println("failed edit : ${t.localizedMessage}")
                    callback.dismissProgress()
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {
                        callback.dismissProgress()
                        val responseMessage = response.body().toString()
                        println("success Edit  :$responseMessage")
                        callback.onSuccess()
                    } else {
                        callback.dismissProgress()
                        val responseMessageError = response.errorBody()?.string()
                        println("error edit  :$responseMessageError")
                    }


                }
            })
    }


    fun uploadPicture(
        picturePath: File?,
        callback1: UploadPicCallback

    ) {
        println("Image name:$picturePath")
        callback1.displayProgress()

        val requestFile: RequestBody =
            RequestBody.create(MediaType.parse("image/jpg"), picturePath!!)
        val body =
            MultipartBody.Part.createFormData("upload", picturePath.name, requestFile)

        val filename: RequestBody =
            RequestBody.create(MediaType.parse("text/plain"), picturePath.name)



        println("file $picturePath")
        if (body != null) {
            uploadImageFileServer(body, callback1, filename)
        }

    }

    private fun uploadImageFileServer(

        body: MultipartBody.Part,
        callback1: UploadPicCallback,
        filename: RequestBody

    ) {


        RestServiceBuilder.apiService?.uploadImageFile(body, filename)
            ?.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, @NonNull t: Throwable) {
                    callback1.dismissProgress()
                    t.message?.let {
                        Log.d("error_upload_image", it)
                        callback1.onUploadError(it)
                    }

                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {
                        callback1.dismissProgress()
                        var path: String? = null
                        var filename1: String? = null

//                        view.dismissProgress()
                        val responseString: String? = response.body()?.string()
                        println("profile response $responseString")

//                        val jsonarray = JSONArray(responseString)
//
//                        for (i in 0 until jsonarray.length()) {
//                            val jsonobject = jsonarray.getJSONObject(i)
//
//                            path = jsonobject.getString("path")
//                            filename1=jsonobject.getString("filename")
//                        }
                        val jsonobject = JSONObject(responseString)
                        path=jsonobject.getString("image")

                        println("path:$path")

                        callback1.uploadSuccess(path!!)

                    } else {
                        callback1.dismissProgress()
                        val responseString: String? = response.errorBody()?.string()
                        println("Response : $responseString")
                        callback1.onUploadError(responseString.toString())
                    }

                }

            })
    }

    fun previewDetails(id: Int, callback2: PreviewDetailsCallback) {
        callback2.displayPreviewProgress()


        RestServiceBuilder.apiService?.getDataFromEditPreviewApi(id.toString())
            ?.enqueue(object : Callback<DummyMOdel> {
                override fun onFailure(call: Call<DummyMOdel>, t: Throwable) {
                    callback2.editPreviewError(t.localizedMessage)
                    println("Failure reason: ${t.localizedMessage}")
                    callback2.dismissPreviewProgress()
                }

                override fun onResponse(
                    call: Call<DummyMOdel>,
                    response: Response<DummyMOdel>
                ) {
                    println("response:$response")
                    if (response.isSuccessful) {
                        callback2.dismissPreviewProgress()
                        val responseMessage = response.body()
                        println("success profile Details${Gson().toJson(response.body())}")
                        println("Success:$responseMessage")
                        responseMessage?.let { callback2.editPreviewSuccess(it) }
                    } else {
                        val errorMessage = response.errorBody().toString()
                        if (errorMessage != null) {
                            callback2.dismissPreviewProgress()
                            callback2.editPreviewError(errorMessage)
                            println("error:$errorMessage")
                        }
                    }

                }

            })


    }


}


