package com.mobileapp.lms.view.leaverequest

import com.mobileapp.lms.Model.LeaveRequest.ApproveLeaveParams
import com.mobileapp.lms.Model.LeaveRequest.Base_class_Leave_Request
import com.mobileapp.lms.Model.LeaveRequest.RejectLeaveRequestParams
import com.mobileapp.lms.Presenter.Api.RestServiceBuilder
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 *  Module name:
 *  created: 3/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
class LeaveRequesrInteractor {

    interface LeaveRequestCallback {

        fun onSuccess(applyList: Base_class_Leave_Request)
        fun onError(error: String)
        fun displayProgress()
        fun dismissProgress()
    }

    interface LeaveapproveCallback {

        fun onApproveSuccess()
        fun onApproveError(error: String)
    }

    interface LeaveRejectCallback {
        fun onRejectSuccess()
        fun onRejectError(error: String)

    }

    fun LeaveRequests(

        callback: LeaveRequestCallback
    ) {
        callback.displayProgress()
        RestServiceBuilder.apiService?.listOfRequests(0)
            ?.enqueue(object : Callback<Base_class_Leave_Request> {
                override fun onFailure(call: Call<Base_class_Leave_Request>, t: Throwable) {
                    callback.dismissProgress()
                    callback.onError(t.localizedMessage)
                    println("Failure:${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<Base_class_Leave_Request>,
                    response: Response<Base_class_Leave_Request>
                ) {
                    if (response.isSuccessful) {
                        callback.dismissProgress()
                        var responsemsg = response.body()?.let { callback.onSuccess(it) }
                        println(message = "Success:$responsemsg")
                    } else {
                        callback.dismissProgress()
                        val errormsg = response.errorBody()?.string()
                        if (errormsg != null) {
                            callback.onError(errormsg)
                            println("error:$errormsg")
                        }
                    }

                }
            })


    }

    fun Leaveapprove(
        id: Int, AppliedId: Int,
        callback1: LeaveapproveCallback
    ) {
        println("ID:$id")
        var leaveapproveparam = ApproveLeaveParams(1, AppliedId)
        RestServiceBuilder.apiService?.approveleave(id, leaveapproveparam)
            ?.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    callback1.onApproveError(t.localizedMessage)
                    println("Failure:${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {
                        var responsemsg = response.body()
                        println("success:$responsemsg")
                        callback1.onApproveSuccess()
                    } else {
                        var responsemsg = response.errorBody()?.string()
                        println("error:$responsemsg")
                        //  callback1.onApproveError(responsemsg)
                    }
                }


            })

    }

    fun LeaveReject(
        empId: Int,
        Appliedid: Int,
        reason: String,
        id: Int,
        callback2: LeaveRejectCallback
    ) {


        var rejectLeaveRequestParams = RejectLeaveRequestParams(id, 2, reason, empId)
        println("employeeId:$empId")
        RestServiceBuilder.apiService?.rejectleave(Appliedid, rejectLeaveRequestParams)
            ?.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    callback2.onRejectError(t.localizedMessage)
                    println("Failure:${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {

                        var responsemsg = response.body()
                        println("success:$responsemsg")
                        callback2.onRejectSuccess()
                    } else {
                        var responsemsg = response.errorBody().toString()
                        println("error:$responsemsg")
                        callback2.onRejectError(responsemsg)

                    }
                }

            })

    }


}