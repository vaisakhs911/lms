package com.mobileapp.lms.view.activityfeeds

import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.mobileapp.lms.R


class DocumentActivity : AppCompatActivity() {
private var path:String?=null
    private var urlWebView:WebView?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_document)
        initView()
        documentSet()


    }

    private fun documentSet() {
        if (path!==""){
            val url="http://cubets3dev.s3.us-west-1.amazonaws.com/$path"
            urlWebView?.webViewClient = WebViewClient()
            urlWebView?.settings?.javaScriptEnabled = true
            urlWebView?.settings?.useWideViewPort = false
            urlWebView?.loadUrl(
                "https://docs.google.com/gview?embedded=true&url=$url"
            )
            urlWebView?.webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView, url: String) {}
            }
        }
    }

    private fun initView() {
        path= intent.getStringExtra("documentPath")
      urlWebView=findViewById(R.id.containWebView)
    }
}
