package com.mobileapp.lms.view.leaverequest

/**
 *  Module name:
 *  created: 13/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
interface LeaveRejectView {

    fun onRejectSuccess()
    fun onRejectError(error:String)
}