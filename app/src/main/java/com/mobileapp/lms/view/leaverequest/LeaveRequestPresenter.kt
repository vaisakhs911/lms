package com.mobileapp.lms.view.leaverequest

import com.mobileapp.lms.Model.LeaveRequest.Base_class_Leave_Request

/**
 *  Module name:
 *  created: 3/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
class LeaveRequestPresenter(
    private val leaveRequestView: LeaveRequestView,
    private val leaveRequestInteractor: LeaveRequesrInteractor,
    private val leaveApproveView: LeaveApproveView,
    private val leaveRejectView: LeaveRejectView
) {
    fun leaveRequests() {


        leaveRequestInteractor.LeaveRequests(
            object : LeaveRequesrInteractor.LeaveRequestCallback {
                override fun onSuccess(applyList: Base_class_Leave_Request) {
                    if (leaveApproveView != null) {
                        leaveRequestView.onSuccess(applyList)
                    }
                }


                override fun onError(error: String) {
                    leaveRequestView.onError(error)

                }

                override fun displayProgress() {
                    leaveRequestView.displayProgress()
                }

                override fun dismissProgress() {
                    leaveRequestView.dismissProgress()
                }
            })
    }

    fun leaveApprove(id: Int, AppliedId: Int) {


        leaveRequestInteractor.Leaveapprove(id, AppliedId,
            object : LeaveRequesrInteractor.LeaveapproveCallback {
                override fun onApproveSuccess() {
                    leaveApproveView.onApproveSuccess()
                }

                override fun onApproveError(error: String) {
                    leaveApproveView.onApproveError(error)
                }


            })

    }

    fun leaveReject(employeeId: Int, Appliedid: Int, reason: String, id: Int) {
        leaveRequestInteractor.LeaveReject(
            employeeId, Appliedid,
            reason, id,
            object : LeaveRequesrInteractor.LeaveRejectCallback {
                override fun onRejectSuccess() {
                    leaveRejectView.onRejectSuccess()
                }

                override fun onRejectError(error: String) {
                    leaveRejectView.onRejectError(error)
                }


            })


    }
}


