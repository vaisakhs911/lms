package com.mobileapp.lms.view.forgotpassword

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.mobileapp.lms.R
import com.mobileapp.lms.view.login.LMSLogin
import kotlinx.android.synthetic.main.activity_forgot_password.*

class ForgotPassword : AppCompatActivity(), ForgotPasswordView {
   private var mForgotPasswordPresenter:ForgotPasswordPresenter?=null
    private var dialog: ProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        dialog = ProgressDialog(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        validation()
        onClickLogin()
mForgotPasswordPresenter= ForgotPasswordPresenter(this, ForgotPasswordInteractor())

    }


    private fun onClickLogin() {
        tv_back_fgp.setOnClickListener {

            val intent = Intent(this, LMSLogin::class.java)
            startActivity(intent)
        }

    }

    private fun validation() {
        rl_change_password_btn_fgp.setOnClickListener {

            if (et_email_address_fgp.text.isNullOrEmpty()) {
                et_email_address_fgp?.error = "Email Address cannot be empty..."
            } else if (!Patterns.EMAIL_ADDRESS.matcher(et_email_address_fgp.text).matches()) {
                Toast.makeText(this, "enter valid mail id....", Toast.LENGTH_SHORT).show()
            } else {

               mForgotPasswordPresenter?.forgotPassword(et_email_address_fgp.text.toString())

            }
        }

    }

    override fun dismissProgress() {
        dialog?.dismiss()
    }

    override fun displayProgress() {
        dialog!!.setMessage("Loading. Please wait...")
        dialog!!.isIndeterminate = true
        dialog?.setCancelable(false)
        dialog!!.show()
    }

    override fun onSuccess() {
        Toast.makeText(this, "Link send to email id..", Toast.LENGTH_SHORT).show()
    }

    override fun onError(error: String) {

    }
}
