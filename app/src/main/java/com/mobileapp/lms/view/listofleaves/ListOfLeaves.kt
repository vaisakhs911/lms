package com.mobileapp.lms.view.listofleaves

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.shimmer.ShimmerFrameLayout
import com.mobileapp.lms.Model.LeaveRequest.Base_class_Leave_Request
import com.mobileapp.lms.Presenter.Adapter.ListLeaveAdapter
import com.mobileapp.lms.R
import kotlinx.android.synthetic.main.activity_list_of_leaves.*


class ListOfLeaves : AppCompatActivity(), ListOfLeavesView {
    private var mShimmerViewContainer: ShimmerFrameLayout? = null    //Initialise it


    private var rlMainLayout: LinearLayout? = null  //the main layout of activity


    lateinit var recyclerView: RecyclerView
    lateinit var context: Context
    private var mListLeavesPresenter: ListOfLeavesPresenter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_of_leaves)
        recyclerView = findViewById(R.id.rv_list_lv)
        recyclerView.layoutManager = LinearLayoutManager(this)

        mListLeavesPresenter = ListOfLeavesPresenter(this, LeaveListInteractor())


        mListLeavesPresenter?.listOfLeavesGet()
        backButtonClick()
        rlMainLayout=findViewById(R.id.main_layout_list_of_leaves)
        mShimmerViewContainer=findViewById(R.id.shimmer_view_container_list_ofLeaves)

    }

    private fun backButtonClick() {
        back_leave_request.setOnClickListener {
            onBackPressed()
        }
    }


    override fun onError(error: String) {
        Toast.makeText(this, "Something went wrong!!", Toast.LENGTH_SHORT).show()

    }

    override fun displayProgress() {
        rlMainLayout?.visibility=View.GONE
        mShimmerViewContainer?.startShimmerAnimation()
        mShimmerViewContainer?.visibility = View.VISIBLE

    }

    override fun dismissProgress() {
        rlMainLayout?.visibility=View.VISIBLE
        mShimmerViewContainer?.stopShimmerAnimation()
        mShimmerViewContainer?.visibility = View.GONE

    }
    override fun onResume() {
        super.onResume()
        mShimmerViewContainer!!.startShimmerAnimation()                  //to avoid background Tasks
    }

    override fun onPause() {
        mShimmerViewContainer!!.stopShimmerAnimation()
        super.onPause()
    }

    override fun onLeaveListedSuccess(applyList: Base_class_Leave_Request) {

        if(applyList!=null && applyList.result!=null) {
            val adapter = ListLeaveAdapter(applyList.result, this)
            recyclerView.adapter = adapter
            adapter.notifyDataSetChanged()

        }else{
            recyclerView.visibility= View.GONE
        }





        println(message = "model:$applyList")

    }

    override fun LeaveApprovedSuccess() {

    }

    override fun LeaveApprovedError(error: String) {
    }

    override fun LeaveRejectedSuccess() {
    }

    override fun LeaveRejectedError(error: String) {
    }


}
