package com.mobileapp.lms.view.login

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.LoginResponseModel
import com.mobileapp.lms.R
import com.mobileapp.lms.Presenter.Utils.CommonUtils
import com.mobileapp.lms.view.HomePage.HomeScreenActivity
import com.mobileapp.lms.view.forgotpassword.ForgotPassword
import kotlinx.android.synthetic.main.activity_lmslogin.*


class LMSLogin : AppCompatActivity(), LoginView {

    private val idConstant="id"
    private val firstName="firstname"
    private val userType="usertype"
    private val employeeId="employeeid"
    private val accessToken="accesstoken"
    private val loginSuccess="loginSuccesskey"
    private var dialog: ProgressDialog? = null
    private var mLoginPresenter: LoginPresenter? = null
    private var email:String?=null
    private var password:String?=null
    var sharedPreferences: SharedPreferences? = null
    private val myPreferences = "LOGIN"
    @SuppressLint("CommitPrefEdits")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialog = ProgressDialog(this)


        setContentView(R.layout.activity_lmslogin)
        validation()
        forgotPassword()
        mLoginPresenter = LoginPresenter(this, LoginInteractor())



    }


    private fun forgotPassword() {
        tv_forgot_password_lgn.setOnClickListener {

            val intent = Intent(this, ForgotPassword::class.java)
            startActivity(intent)
        }
    }

    private fun validation() {

        rlayout_login_lgn.setOnClickListener {

            if (et_email_address_lgn.text.isNullOrEmpty()) {
                et_email_address_lgn?.error = "Email Address cannot be empty..."
            } else if (!Patterns.EMAIL_ADDRESS.matcher(et_email_address_lgn.text).matches()) {
                Toast.makeText(this, "enter valid mail id....", Toast.LENGTH_SHORT).show()
            } else if (et_password_lgn?.text.isNullOrEmpty()) {
                et_password_lgn?.error = "Password cannot be empty."
            } else if (et_password_lgn.length() < 7) {
                Toast.makeText(this, "password length should be greater than 6", Toast.LENGTH_LONG)
                    .show()
            } else {


                mLoginPresenter?.login(

                    et_email_address_lgn.text.toString(),
                    et_password_lgn.text.toString()
                )

                }
                println("email:${et_email_address_lgn.text}")
                CommonUtils.hideKeyBoard(this)
        }


        }


    private fun rememberMe(){
        var isChecked:Boolean=cb_remember_me_lgn.isChecked
//        cb_remember_me_lgn.setOnCheckedChangeListener { _, isChecked ->

          /*  et_email_address_lgn.setText(email)
            et_password_lgn.setText(password)*/
            sharedPreferences = getSharedPreferences(myPreferences, Context.MODE_PRIVATE)
            val editor = sharedPreferences?.edit()
            val s: Boolean = sharedPreferences?.getBoolean("loginSuccesskey",false)!!
//            val id:Int= sharedPreferences?.getInt("id",0)!!
//            println("rememberid:$id")
            editor?.putBoolean(loginSuccess,false)
            editor?.apply()
            print("default:$s")
            if (!et_email_address_lgn.text.isNullOrEmpty() &&  !et_password_lgn.text.isNullOrEmpty()){

                if (isChecked) {
                    editor?.putBoolean(loginSuccess,true)
                    editor?.apply()
                    val s: Boolean = sharedPreferences?.getBoolean("loginSuccesskey",false)!!
                    println("checked:clickeddddd..............")
                    println("statusTRUE:$s")
                }else{
                    editor?.putBoolean(loginSuccess,false)
                    editor?.apply()
                    val s: Boolean = sharedPreferences?.getBoolean("loginSuccesskey",false)!!
                    println("statusFALSE:$s")
                    println("notchecked:clickeddddd..............")
                }

            }

//        }



    }

    override fun onError(error: String) {
        Toast.makeText(this, "Check your email or Password ", Toast.LENGTH_SHORT).show()
    }

    override fun displayProgress() {
       dialog!!.dismiss()
    }

    override fun dismissProgress() {
        dialog!!.setMessage("Loading. Please wait...")
        dialog!!.isIndeterminate = true
        dialog?.setCancelable(false)
        dialog!!.show()

    }

    override fun onSuccess(loginLog: LoginResponseModel) {
        rememberMe()
        Toast.makeText(this, "Login Successfully...", Toast.LENGTH_SHORT).show()

        val name=loginLog.result!![0].firstName
        val id=loginLog.result!![0].id
        var employeeId=loginLog.result!![0].employeeId
        var accessToken=loginLog.acesstoken
        var usertype=loginLog.result!![0].userType
        println("acccesstoken:${loginLog.acesstoken}")
        println("id:${loginLog.result!![0].id}")
        sharedPreferences = getSharedPreferences(myPreferences, Context.MODE_PRIVATE)
        val editor = sharedPreferences?.edit()
        editor?.putInt(idConstant, id!!)
        editor?.putString(firstName,name)
        editor?.putInt(userType,usertype!!)
        editor?.putString(this.employeeId,employeeId)
        editor?.putString(this.accessToken,accessToken)
        editor?.apply()

        val intent = Intent(this, HomeScreenActivity::class.java)
        startActivity(intent)
        finish()
    }
}