package com.mobileapp.lms.view


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.mobileapp.lms.R
import com.mobileapp.lms.view.applyleave.ApplyLeaveActivity
import com.mobileapp.lms.view.leaverequest.LeaveRequest
import com.mobileapp.lms.view.listofleaves.ListOfLeaves

/**
 * A simple [Fragment] subclass.
 */
@Suppress("UNREACHABLE_CODE")
class LeaveManagementFragment : Fragment() {

    private var applyButton: RelativeLayout? = null
    private var leaveRequestButton: RelativeLayout? = null
    private var leaveListButton: RelativeLayout? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_leave_management, container, false)
        initView()
         applyButton = view.findViewById(R.id.rl_apply_leave)
         leaveRequestButton = view.findViewById(R.id.rl_leave_requests)
         leaveListButton =view.findViewById(R.id.rl_list_of_leaves)
        applyButton?.let { leaveApplyButton(it) }
        leaveRequestButtonClick(leaveRequestButton!!)
        leaveListButtonClick(leaveListButton!!)
        return view
    }

    private fun leaveApplyButton(applyButton: RelativeLayout) {
        applyButton.setOnClickListener {

            context?.let {
                val intent = Intent(context, ApplyLeaveActivity::class.java)
                context?.startActivity(intent)
            }
        }
    }


    private fun initView() {
        val heading = activity?.findViewById<TextView>(R.id.tv_heading_hme_screen)
        val notificationCount =
            activity?.findViewById<RelativeLayout>(R.id.rl_count_of_notificatns_feeds)
        val icNotification = activity?.findViewById<ImageView>(R.id.img_notifications_active_feeds)
        val icEdit = activity?.findViewById<ImageView>(R.id.edit_profile_icon)
        val icSearch = activity?.findViewById<ImageView>(R.id.img_searchbtn_activityfeeds)

        visibilityOfItems(heading!!, notificationCount!!, icNotification!!, icEdit!!, icSearch!!)



    }

    private fun visibilityOfItems(
        heading: TextView,
        notificationCount: RelativeLayout,
        icNotification: ImageView,
        icEdit: ImageView,
        icSearch: ImageView
    ) {
        heading.text = "Leave Management"
        notificationCount.visibility = View.GONE
        icNotification.visibility = View.GONE
        icEdit.visibility = View.GONE
        icSearch.visibility = View.GONE
    }

    private fun leaveListButtonClick(leaveListButton: RelativeLayout) {
        leaveListButton.setOnClickListener {
            val intent = Intent(activity, ListOfLeaves::class.java)
            activity!!.startActivity(intent)
        }
    }



    private fun leaveRequestButtonClick(leaveRequestButton: RelativeLayout) {

        leaveRequestButton.setOnClickListener {

            val intent = Intent(activity, LeaveRequest::class.java)
            activity!!.startActivity(intent)
        }

    }


}
