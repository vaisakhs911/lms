package com.mobileapp.lms.view.activityfeeds

import com.mobileapp.lms.Model.ActivityFeeds.feedlist.FeedListModel

/**
 *  Module name:
 *  created: 12/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
interface ActivityFeedsView {
    fun dismissProgress()
    fun displayProgress()
    fun onError(error:String)
    fun onSuccess(activityfeeds: FeedListModel)
}