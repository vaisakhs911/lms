package com.mobileapp.lms.view.editProfile


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.facebook.shimmer.ShimmerFrameLayout
import com.mobileapp.lms.Model.dummyMOdel.DummyMOdel
import com.mobileapp.lms.R
import com.mobileapp.lms.Presenter.Utils.ImagePickerUtil
import com.mobileapp.lms.Presenter.Utils.TimeUtils
import com.mobileapp.lms.view.HomePage.HomeScreenActivity
import kotlinx.android.synthetic.main.activity_edit_profile.*
import java.io.File
import java.util.*


class EditProfileActivity : AppCompatActivity(), EditProfileView {
    private var dialog: ProgressDialog? = null
    private var mShimmerViewContainer: ShimmerFrameLayout? = null
    private var rlMainLayout: RelativeLayout? = null

    private var mEditProfilePresenter: EditProfilePresenter? = null
    private var cal: Calendar
        get() = Calendar.getInstance()
        set(value) = TODO()
    private var imageUri: File? = null
    private var uploadedImagePath: String? = null
    private var sharedPreferences: SharedPreferences? = null
    private val myPreferences = "LOGIN"

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        dialog = ProgressDialog(this)
        dateShow()
        imageUploadButtonClick()
        /*  pictureUpload()*/
        sharedPreferences = getSharedPreferences(myPreferences, Context.MODE_PRIVATE)
        val id: Int = sharedPreferences?.getInt("id", 0)!!
        cancelButtonClick()
        mEditProfilePresenter = EditProfilePresenter(
            this, EditProfileInteractor(this)
        )
        validation(id)
        sendDataToPresenterPreview(id)
        initView()
    }

    private fun initView() {
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container)
        rlMainLayout = findViewById(R.id.main_layout_editProfile)
    }


    private fun cancelButtonClick() {
        rl_cancel_edit_profile.setOnClickListener {
            onBackPressed()
        }
    }

    private fun sendDataToPresenterPreview(id: Int) {
        mEditProfilePresenter?.previewDetails(id)

    }

    /*private fun pictureUpload() {
        img_upload_my_profile.setOnClickListener {
            if (imageUri != null) {
                mEditProfilePresenter?.uploadPic(imageUri)
                println("Image name:$imageUri")
            }
        }
        backButtonClick()

    }*/

    private fun backButtonClick() {
        image_back_edit_profile.setOnClickListener {
            onBackPressed()
        }
    }


    private fun validation(id: Int) {

        rl_btn_save_edit_profile.setOnClickListener {
            when {

                et_full_name_edit_profile.text.isNullOrEmpty() -> {
                    et_full_name_edit_profile.error = "Please enter the First name"
                }
                et_contact_no_edit_profile.text.isNullOrEmpty() -> {
                    et_contact_no_edit_profile.error = "Please enter the phone number"
                }
                et_contact_no_edit_profile.length() != 10 -> {
                    Toast.makeText(this, "Please enter valid phone number", Toast.LENGTH_LONG)
                        .show()
                }
                et_bld_grp_edit_profile.text.isNullOrEmpty() -> {
                    et_bld_grp_edit_profile.error = "Please enter the Blood group.."
                }
                tv_dob_edit_profile.text.isNullOrEmpty() -> {
                    tv_dob_edit_profile.error = "Please enter Date Of Birth.."
                }
                et_address1_edit_profile.text.isNullOrEmpty() -> {
                    et_address1_edit_profile.error = "Please enter the Address line 1.."
                }
                et_address2_edit_profile.text.isNullOrEmpty() -> {
                    et_address2_edit_profile.error = "Please enter the Address line 2.."
                }


                et_postal_code_profile.text.isNullOrEmpty() -> {
                    et_postal_code_profile.error = "Please enter the Postal code.."
                }

                et_country_edit_profile.text.isNullOrEmpty() -> {
                    et_country_edit_profile.error = "Please enter the Country.."
                }
                else -> {

                    sendDataToPresenterPatch(id)
                    launchHomeScreen()

                }

            }

        }

    }

    private fun launchHomeScreen() {
        val intent = Intent(this, HomeScreenActivity::class.java)
        intent.putExtra("registered", true)
        startActivity(intent)

    }

    private fun sendDataToPresenterPatch(id: Int) {


        mEditProfilePresenter?.editProfilePatch(
            id,
            et_full_name_edit_profile.text.toString(),
            et_postal_code_profile.text.toString(),
            tv_dob_edit_profile.text.toString(),
            et_address1_edit_profile.text.toString(),
            et_address2_edit_profile.text.toString(),
            et_country_edit_profile.text.toString(),
            et_contact_no_edit_profile.text.toString(),
            et_bld_grp_edit_profile.text.toString(),
            uploadedImagePath
        )
        val address2 = et_address2_edit_profile.text.toString()
        println("the value:$address2")
        Log.d("Image path ", "test --> $uploadedImagePath")
    }


    private fun imageUploadButtonClick() {
        img_upload_my_profile.setOnClickListener {
            imagePic()


        }
    }

    private fun imagePic() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED
            ) {

                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                requestPermissions(
                    permissions,
                    PERMISSION_CODE
                )
            } else {
                pickImageFromGallery()
            }

        } else {
            pickImageFromGallery()
        }

    }


    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(
            intent,
            IMAGE_PICK_CODE
        )

    }

    companion object {
        //image pick code
        const val IMAGE_PICK_CODE = 1000
        //Permission code
        const val PERMISSION_CODE = 1001
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    //permission from popup granted
                    pickImageFromGallery()
                } else {
                    //permission from popup denied
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            img_upload_my_profile.setImageURI(data?.data)
            val filePath: String? = ImagePickerUtil.getRealPathFromUri(this, data?.data)
            imageUri = File(filePath)
            if (imageUri != null) {
                mEditProfilePresenter?.uploadPic(imageUri)
            }
            backButtonClick()
        }
    }


    @SuppressLint("SetTextI18n")
    private fun dateShow()                           //function to get date and show it
    {
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val month = monthOfYear + 1
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)

                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                if (monthOfYear < 10) {
                    tv_dob_edit_profile.text = "$dayOfMonth/0$month/$year"
                } else {
                    tv_dob_edit_profile.text = " $dayOfMonth/$month/$year"
                }
                println(tv_dob_edit_profile)

                Log.d("date", "$tv_dob_edit_profile")
            }
        tv_dob_edit_profile!!.setOnClickListener {
            DatePickerDialog(
                this@EditProfileActivity,
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            ).show()
        }
    }

    override fun dismissProgress() {
        dialog?.dismiss()
    }

    override fun displayProgress() {
        dialog!!.setMessage("Loading. Please wait...")
        dialog!!.isIndeterminate = true
        dialog?.setCancelable(false)
        dialog!!.show()
    }

    override fun onSuccess() {
        Toast.makeText(this, "Data saved successfully", Toast.LENGTH_SHORT).show()
    }

    override fun onError(error: Any?) {
        Toast.makeText(this, "Something went wrong!!", Toast.LENGTH_SHORT).show()
    }

    override fun uploadSuccess(path: String) {


        Log.d("Image path ", "Activity --> $path")
        uploadedImagePath = path
        Log.d("Image path ", "Activity --> $uploadedImagePath")
        Toast.makeText(this, "Profile image successfully uploaded!", Toast.LENGTH_SHORT).show()
    }

    override fun onUploadError(error: String) {
        Toast.makeText(this, "Something went wrong!!", Toast.LENGTH_SHORT).show()

    }

    @SuppressLint("DefaultLocale")
    override fun editPreviewSuccess(profileList: DummyMOdel) {
        val image = profileList.result!![0].photo

        val address = profileList.result!![0].address
        println("address:$address")
        val dateOfBirth = profileList.result!![0].dOB?.let {
            TimeUtils.convertServerFormatTodatpickerFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", it
            )
        }
        et_full_name_edit_profile.setText(profileList.result!![0].firstname?.toUpperCase())
        et_contact_no_edit_profile.setText(profileList.result!![0].phone)
        et_bld_grp_edit_profile.setText(profileList.result!![0].bloodGroup)
        et_address1_edit_profile.setText(address)
        et_address2_edit_profile.setText(profileList.result!![0].address2)
        tv_dob_edit_profile.text = dateOfBirth
        et_country_edit_profile.setText(profileList.result!![0].country)
        et_postal_code_profile.setText(profileList.result!![0].pincode)

        val into = Glide
            .with(this)
            .load("https://cubets3dev.s3.us-west-1.amazonaws.com/" + image)

            .into(img_upload_my_profile)
    }

    override fun editPreviewError(error: String) {
        Toast.makeText(this, "Something went wrong!!", Toast.LENGTH_SHORT).show()
    }

    override fun displayPreviewProgress() {
        rlMainLayout?.visibility = View.GONE
        mShimmerViewContainer?.startShimmerAnimation()
        mShimmerViewContainer?.visibility = View.VISIBLE

    }

    override fun dismissPreviewProgress() {
        rlMainLayout?.visibility = View.VISIBLE
        mShimmerViewContainer?.stopShimmerAnimation()
        mShimmerViewContainer?.visibility = View.GONE
    }


    override fun onResume() {
        super.onResume()
        mShimmerViewContainer!!.startShimmerAnimation()                  //to avoid background Tasks
    }

    override fun onPause() {
        mShimmerViewContainer!!.stopShimmerAnimation()
        super.onPause()
    }
}