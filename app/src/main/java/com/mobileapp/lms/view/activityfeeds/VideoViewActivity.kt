package com.mobileapp.lms.view.activityfeeds

import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.view.KeyEvent
import android.widget.MediaController
import android.widget.Toast
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import com.mobileapp.lms.R


class VideoViewActivity : AppCompatActivity() {
    private var videoView: VideoView? = null
    private var path: String? = null
    private var mediaController: MediaController? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_view)
        initView()

    }

    private fun initView() {
        videoView = findViewById(R.id.video_post_video_feeds)
        path = intent.getStringExtra("videoPath")
        videoViewSett()
    }

    private fun videoViewSett() {

        if (path !== null) {
            println("PATH:$path")
            mediaController = MediaController(this)
            mediaController!!.setAnchorView(videoView)
            videoView!!.setMediaController(mediaController)
            videoView!!.setVideoURI(Uri.parse("https://cubets3dev.s3.us-west-1.amazonaws.com/$path"))
            videoView!!.start()
            videoView!!.requestFocus()
            videoView!!.setOnPreparedListener(MediaPlayer.OnPreparedListener { mp ->
                mp.setOnVideoSizeChangedListener { mp, width, height ->
                    videoView!!.setMediaController(mediaController)
                    mediaController!!.setAnchorView(videoView)
                }
            })

        }

    }

    override fun onBackPressed() { // code here to show dialog
        super.onBackPressed() // optional depending on your needs
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {

            videoView!!.setOnCompletionListener { mp ->
                Toast.makeText(applicationContext, "Video over", Toast.LENGTH_SHORT).show()

                mp.release()
                Toast.makeText(applicationContext, "Video over", Toast.LENGTH_SHORT)
                    .show()

            }
            onBackPressed()

            //do whatever you need for the hardware 'back' button
            true
        } else super.onKeyDown(keyCode, event)
    }
}
