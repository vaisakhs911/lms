package com.mobileapp.lms.view.forgotpassword

/**
 *  Module name:
 *  created: 5/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
interface ForgotPasswordView {
    fun dismissProgress()
    fun displayProgress()
    fun onSuccess()
    fun onError(error:String)


}