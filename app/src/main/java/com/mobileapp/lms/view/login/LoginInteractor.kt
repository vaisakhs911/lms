package com.mobileapp.lms.view.login

import com.example.LoginRequestModel
import com.example.LoginResponseModel
import com.google.gson.Gson
import com.mobileapp.lms.Presenter.Api.RestServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 *  Module name:
 *  created: 18/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
class LoginInteractor {
    interface logincallback {
        fun onSuccess(LoginLog: LoginResponseModel)
        fun onError(error: String)
        fun displayProgress()
        fun dismissProgress()
    }

    fun login(email: String, password: String, callback: logincallback) {
       /* callback.displayProgress()*/
        println("email:$email")
        var loginrequestmodel = LoginRequestModel()
        loginrequestmodel.email = email
        loginrequestmodel.password = password
        println("the posted mail:$email")
        println("the posted mail:$password")
        RestServiceBuilder.apiService?.login(loginrequestmodel)
            ?.enqueue(object : Callback<LoginResponseModel> {
                override fun onFailure(call: Call<LoginResponseModel>, t: Throwable) {
                    callback.dismissProgress()
                    println("failed login : ${t.localizedMessage}")

                }

                override fun onResponse(
                    call: Call<LoginResponseModel>,
                    response: Response<LoginResponseModel>
                ) {
                    if (response.isSuccessful) {
                        /*callback.dismissProgress()*/
                        println("hai:${response.body()}")
                        var responsemsg = response.body()
                        responsemsg?.let { callback.onSuccess(it) }

                        println("success login:  $responsemsg")
                        println("success login response${Gson().toJson(response.body())}")

                    } else {
                      /*  callback.dismissProgress()*/
                        var responsemsg = response.errorBody()?.toString()
                        println("error login:$responsemsg")
                        if (responsemsg != null) {
                            callback.onError(responsemsg)
                        }
                    }


                }


            })


    }
}