package com.mobileapp.lms.view.listofleaves

import com.mobileapp.lms.Model.LeaveRequest.Base_class_Leave_Request

/**
 *  Module name:
 *  created: 30/1/20
 *  Author: AJITH TS
 *  Revisions:
 */


class ListOfLeavesPresenter(
    private val listOfLeaveView: ListOfLeavesView,
    private var listOfLeaveInteractor: LeaveListInteractor
) {
    fun listOfLeavesGet() {
        listOfLeaveInteractor.listOfLeavesGet(object :LeaveListInteractor.ListOfLeaveCallback{
            override fun onSuccess(applyList: Base_class_Leave_Request) {
                listOfLeaveView.onLeaveListedSuccess(applyList)
            }

            override fun onError(error: String) {
               listOfLeaveView.onError(error)
            }

            override fun displayProgress() {
                listOfLeaveView.displayProgress()
            }

            override fun dismissProgress() {
                listOfLeaveView.dismissProgress()
            }

        })
    }




    fun leaveApprovePost(id:Int, AppliedId:Int){
        listOfLeaveInteractor.leaveApprovePost(id,AppliedId,object : LeaveListInteractor.LeaveApproveCallback{
            override fun onApproveSuccess() {
                listOfLeaveView.LeaveApprovedSuccess()
            }

            override fun onApproveError(error: String) {
                listOfLeaveView.LeaveApprovedError(error)
            }


        })


    }


    fun leaveRejectPost(EmployeeId:Int, AppliedId:Int, id:Int){

        listOfLeaveInteractor.leaveRejectPost(EmployeeId,AppliedId,"tt",id,object :LeaveListInteractor.LeaveRejectCallback{
            override fun onRejectSuccess() {
                listOfLeaveView.LeaveRejectedSuccess()

            }

            override fun onRejectError(error: String) {
                listOfLeaveView.LeaveRejectedError(error)
            }


        })
    }
}