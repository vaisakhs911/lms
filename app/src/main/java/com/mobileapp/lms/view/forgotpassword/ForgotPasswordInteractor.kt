package com.mobileapp.lms.view.forgotpassword

import com.mobileapp.lms.Model.ForgotPasswordParam.ForgottPasswordParam
import com.mobileapp.lms.Presenter.Api.RestServiceBuilder
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response

/**
 *  Module name:
 *  created: 5/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
class ForgotPasswordInteractor {


    interface ForgotPasswordCallback {
        fun onSuccess()
        fun onError(error: String)
        fun displayProgress()
        fun dismissProgress()
    }

    fun ForgotPassword(
        email: String,
        callback: ForgotPasswordCallback

    ) {
        callback.displayProgress()

        var forgotpasswordparam =
            ForgottPasswordParam(
                email
            )
        RestServiceBuilder.apiService?.forgotpassword(forgotpasswordparam)
            ?.enqueue(object : retrofit2.Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    callback.dismissProgress()
                    println("failed : ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {
                        callback.dismissProgress()
                        var responsemsg = response.body().toString()
                        println("Success:$responsemsg")
                        callback.onSuccess()
                    } else {
                        callback.dismissProgress()
                        var responsemsgerror = response.errorBody().toString()
                        println("Error:$responsemsgerror")
                        callback.onError(responsemsgerror)
                    }


                }

            })
    }
}