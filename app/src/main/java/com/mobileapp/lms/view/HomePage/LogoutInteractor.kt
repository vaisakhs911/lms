package com.mobileapp.lms.view.HomePage

import com.google.gson.Gson
import com.mobileapp.lms.Model.dummyMOdel.DummyMOdel
import com.mobileapp.lms.Model.logout.LogoutResponseModel
import com.mobileapp.lms.Presenter.Api.RestServiceBuilder
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 *  Module name:
 *  created: 24/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
class LogoutInteractor {

    interface logoutcallback {
        fun onSuccess()
        fun onError(error: String)
        fun displayLogoutProgress()
        fun dismissLogoutProgress()
    }

    interface DisplayDataCallback {

        fun onSuccess(profileList: DummyMOdel)
        fun onError(error: String)
        fun displayProgress()
        fun dismissProgress()
    }

    fun logout(id: Int, callback: LogoutInteractor.logoutcallback) {
        callback.displayLogoutProgress()
        var logoutrequest = LogoutResponseModel()
        logoutrequest.id = id.toLong()
        RestServiceBuilder.apiService?.logout(logoutrequest)
            ?.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    callback.dismissLogoutProgress()
                    println("failed login : ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {
                        callback.dismissLogoutProgress()
                        println("hai:${response.body()}")
                        var responsemsg = response.body()
                        callback.onSuccess()

                        println("success login:  $responsemsg")
                        println("success login response${Gson().toJson(response.body())}")

                    } else {
                        callback.dismissLogoutProgress()
                        var responsemsg = response.errorBody()?.toString()
                        println("error login:$responsemsg")
                        if (responsemsg != null) {
                            callback.onError(responsemsg)
                        }
                    }
                }

            })
    }


    fun displayData(id: Int, callback: DisplayDataCallback) {

        println("IDD:$id")
        RestServiceBuilder.apiService?.profile(id.toString())
            ?.enqueue(object : Callback<DummyMOdel> {
                override fun onFailure(call: Call<DummyMOdel>, t: Throwable) {

                    callback.onError(t.localizedMessage)

                    println("Failure reason: ${t.localizedMessage}").toString()
                }

                override fun onResponse(
                    call: Call<DummyMOdel>,
                    response: Response<DummyMOdel>
                ) {
                    println("response:$response")
                    if (response.isSuccessful) {

                        val successResponseMessage = response.body()
                        println("success profile Details${Gson().toJson(response.body())}")
                        println("Success:$successResponseMessage")
                        successResponseMessage?.let { callback.onSuccess(successResponseMessage) }
                    } else {

                        var errorResponseMessage = response.errorBody().toString()
                        callback.onError(errorResponseMessage)
                        println("error:$errorResponseMessage")
                    }

                }

            })
    }

}
