package com.mobileapp.lms.view.applyleave

import com.mobileapp.lms.Model.ApplyLeaveParam.ApplyLeaveParam
import com.mobileapp.lms.Model.ApplyLeaveParam.ApplyleaveResponseModel
import com.mobileapp.lms.Presenter.Api.RestServiceBuilder
import com.mobileapp.lms.Presenter.Utils.TimeUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 *  Module name:
 *  created: 28/1/20
 *  Author: AJITH TS
 *  Revisions:
 */
class ApplyLeaveInteractor {


    interface ApplyLeaveCallback {

        fun onSuccess(applyResponse:ApplyleaveResponseModel)
        fun onError(error: String)
        fun displayApplyProgress()
        fun dismissApplyProgress()
    }



    fun applyLeavePostInteractor(
        id: Int,
        name: String,
        leaveType: Int,
        leavePattern: Int,
        numberOfDaysRequired: String,
        from: String,
        to: String,
        reason: String,
        callback: ApplyLeaveCallback
    ) {

/*callback.displayApplyProgress()*/
        val fromDate = TimeUtils.convertTimeToServerFormat("dd/MM/yyyy", from)
        println("FromDate :$fromDate")

        val toDate = TimeUtils.convertTimeToServerFormat("dd/MM/yyyy", to)
        println("ToDate :$toDate")

        val applyLeaveParam = ApplyLeaveParam(
            id,
            name,
            leaveType,
            leavePattern,
            fromDate,
            toDate,
            numberOfDaysRequired.toInt(),
            reason,
            0,
            1,
            1,
            1
        )

        RestServiceBuilder.apiService?.postApplyParamsToApi(applyLeaveParam)
            ?.enqueue(object : Callback<ApplyleaveResponseModel> {
                override fun onFailure(call: Call<ApplyleaveResponseModel>, t: Throwable) {
                    /*callback.dismissApplyProgress()*/
                    println("failed : ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<ApplyleaveResponseModel>,
                    response: Response<ApplyleaveResponseModel>
                ) {
                    if (response.isSuccessful) {
                        /*callback.dismissApplyProgress()*/
                        val responseStr = response.body()
                        println("success  :$responseStr")
                        responseStr?.let { callback.onSuccess(it) }
                    } else {
                        /*callback.dismissApplyProgress()*/
                        val responseStr = response.errorBody()?.string()
                        println("error : $responseStr")
                        callback.onError(responseStr.toString())
                    }

                }

            })


    }




}