package com.mobileapp.lms.view.login

import com.example.LoginResponseModel


/**
 *  Module name:
 *  created: 18/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
class LoginPresenter(
    private val loginView: LoginView,
    private val loginInteractor: LoginInteractor
) {


    fun login(email:String, Password:String) {
        println("email:$email")
        loginInteractor.login(email,Password,object : LoginInteractor.logincallback {
            override fun onSuccess(LoginLog: LoginResponseModel) {
                loginView.onSuccess(LoginLog)
            }

            override fun onError(error:String) {
                loginView.onError(error)
            }

            override fun displayProgress() {
                loginView.displayProgress()
            }

            override fun dismissProgress() {
               loginView.dismissProgress()
            }


        })


    }
}