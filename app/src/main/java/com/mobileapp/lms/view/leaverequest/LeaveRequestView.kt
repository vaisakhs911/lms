package com.mobileapp.lms.view.leaverequest

import com.mobileapp.lms.Model.LeaveRequest.Base_class_Leave_Request

/**
 *  Module name:
 *  created: 3/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
interface LeaveRequestView {
    fun onError(error: String)
    fun onSuccess(applyList: Base_class_Leave_Request)
    fun dismissProgress()
    fun displayProgress()


}