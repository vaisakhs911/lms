package com.mobileapp.lms.view.editProfile

import android.util.Log
import com.mobileapp.lms.Model.dummyMOdel.DummyMOdel
import java.io.File

/**
 *  Module name:
 *  created: 5/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
class EditProfilePresenter(
    private var editProfileView: EditProfileView,
    private var editProfileInteractor: EditProfileInteractor
) : EditProfileInteractor.EditProfileCallback, EditProfileInteractor.UploadPicCallback,
    EditProfileInteractor.PreviewDetailsCallback {
    fun editProfilePatch(id:Int,
                         FirstName: String,

                         pinCode: String,
                         dob: String,
                         address1: String,
                         address2: String,
                         country: String,
                         Phone: String,
                         bloodGroup: String,
                         uploadedImagePath: String?
    )
    {
        editProfileInteractor.editProfilePatch(id,
            FirstName,
            pinCode,
            dob,
            address1,
            address2,
            country, Phone, bloodGroup,
            uploadedImagePath,
            this
        )
        println("the value:$address2")
       println("Image path : $uploadedImagePath")
    }


    fun uploadPic(
        photo: File?
    ) {
        println("Image name:$photo")
        editProfileInteractor.uploadPicture(photo, this)

    }
fun previewDetails(id:Int){
    editProfileInteractor.previewDetails(id,this)

}
    override fun onSuccess() {
        editProfileView.onSuccess()
    }

    override fun onError(error: String) {
        editProfileView.onError(error)
    }

    override fun uploadSuccess(path: String) {
        editProfileView.uploadSuccess(path)
        println("path:$path")
    }

    override fun onUploadError(error: String) {
        editProfileView.onUploadError(error)
    }

    override fun editPreviewSuccess(profileList: DummyMOdel) {
        editProfileView.editPreviewSuccess(profileList)
    }

    override fun editPreviewError(error: String) {
        editProfileView.editPreviewError(error)
    }

    override fun displayPreviewProgress() {
        editProfileView.displayPreviewProgress()
    }

    override fun dismissPreviewProgress() {
        editProfileView.dismissPreviewProgress()
    }

    override fun displayProgress() {
        editProfileView.displayProgress()
    }

    override fun dismissProgress() {
        editProfileView.dismissProgress()
    }

}