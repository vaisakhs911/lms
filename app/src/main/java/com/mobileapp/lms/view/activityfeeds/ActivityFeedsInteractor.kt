package com.mobileapp.lms.view.activityfeeds

import com.mobileapp.lms.Model.ActivityFeeds.feedlist.FeedListModel
import com.mobileapp.lms.Presenter.Api.RestServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 *  Module name:
 *  created: 12/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
class ActivityFeedsInteractor {
    interface ActivityFeedsCallback {
        fun dismissProgress()
        fun displayProgress()
        fun onSuccess(activityFeeds: FeedListModel)
        fun onError(error: String)

    }

    fun activityFeeds(Id:Int,
        callback: ActivityFeedsCallback
    ) {
        callback.displayProgress()
        RestServiceBuilder.apiService?.activityFeedsDisplay(Id)
            ?.enqueue(object : Callback<FeedListModel> {
                override fun onFailure(call: Call<FeedListModel>, t: Throwable) {
                    callback.onError(t.localizedMessage)
                    callback.dismissProgress()
                    println("Failurelist:${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<FeedListModel>,
                    response: Response<FeedListModel>
                ) {
                    if (response.isSuccessful) {
                        callback.dismissProgress()
                        var responsemsg = response.body()?.let { callback.onSuccess(it) }
                        println("Success:$responsemsg")

                    } else {
                        callback.dismissProgress()
                        var responsemsg = response.errorBody()?.string()
                        println("error:$responsemsg")
                        callback.onError(responsemsg!!)
                    }
                }
            })
    }


    fun activityFeedsOrder(Id:Int,Order:String, callback: ActivityFeedsCallback){
        println("activity:$Order")
        callback.displayProgress()
        RestServiceBuilder.apiService?.activityFeedsOrder(Id,Order)

            ?.enqueue(object : Callback<FeedListModel> {
                override fun onFailure(call: Call<FeedListModel>, t: Throwable) {
                    callback.onError(t.localizedMessage)
                    callback.dismissProgress()
                    println("Failureorder:${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<FeedListModel>,
                    response: Response<FeedListModel>
                ) {
                    if (response.isSuccessful) {
                        callback.dismissProgress()
                        var responsemsg = response.body()?.let { callback.onSuccess(it) }
                        println("Success:$responsemsg")

                    } else {
                        callback.dismissProgress()
                        var responsemsg = response.errorBody()?.string()
                        println("error:$responsemsg")
                        callback.onError(responsemsg!!)
                    }
                }
            })
    }
    fun activityFeedsSearch(Id:Int,query:String, callback: ActivityFeedsCallback){
        callback.displayProgress()
        RestServiceBuilder.apiService?.activityFeedsSearch(Id,query)
            ?.enqueue(object : Callback<FeedListModel> {
                override fun onFailure(call: Call<FeedListModel>, t: Throwable) {
                    callback.onError(t.localizedMessage)
                    callback.dismissProgress()
                    println("Failuresearch:${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<FeedListModel>,
                    response: Response<FeedListModel>
                ) {
                    if (response.isSuccessful) {
                        callback.dismissProgress()
                        var responsemsg = response.body()?.let { callback.onSuccess(it) }
                        println("Success:$responsemsg")

                    } else {
                        callback.dismissProgress()
                        var responsemsg = response.errorBody()?.string()
                        println("error:$responsemsg")
                        callback.onError(responsemsg!!)
                    }
                }
            })
    }

}