package com.mobileapp.lms.view.applyleave

import com.mobileapp.lms.Model.ApplyLeaveParam.ApplyleaveResponseModel
import com.mobileapp.lms.Model.dummyMOdel.DummyMOdel

/**
 *  Module name:
 *  created: 28/1/20
 *  Author: AJITH TS
 *  Revisions:
 */
class ApplyLeavePresenter(
    private val applyLeaveView: ApplyLeaveView,
    private var applyLeaveInteractor: ApplyLeaveInteractor
) {
    fun applyLeavePostPresenter(
        id: Int,
        name: String,
        leaveType: Int,
        leavePattern: Int,
        numberOfDaysRequired: String,
        from: String,
        to: String,
        reason: String
    ) {
        applyLeaveInteractor.applyLeavePostInteractor(id, name,
            leaveType,
            leavePattern,
            numberOfDaysRequired,
            from,
            to,
            reason,
            object : ApplyLeaveInteractor.ApplyLeaveCallback {
                override fun onSuccess(applyResponse: ApplyleaveResponseModel) {
                    applyLeaveView.onLeaveAppliedSuccess(applyResponse)
                }

                override fun onError(error: String) {
                    applyLeaveView.onError(error)
                }

                override fun displayApplyProgress() {
                    applyLeaveView.displayProgress()
                }

                override fun dismissApplyProgress() {
                    applyLeaveView.dismissProgress()
                }

            })
    }





}