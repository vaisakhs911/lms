package com.mobileapp.lms.view.profile

import com.mobileapp.lms.Model.dummyMOdel.DummyMOdel

/**
 *  Module name:
 *  created: 4/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
interface ProfileView {
    fun dismissProgress()
    fun displayProgress()
    fun onError(error:String)
    fun onSuccess(profileList: DummyMOdel)

}