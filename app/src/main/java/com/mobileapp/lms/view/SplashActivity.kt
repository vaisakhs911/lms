package com.mobileapp.lms.view

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.mobileapp.lms.R
import com.mobileapp.lms.view.HomePage.HomeScreenActivity
import com.mobileapp.lms.view.login.LMSLogin

class SplashActivity : AppCompatActivity() {
    /* var SharedPreferences: SharedPreferences? = null
     val mypreferences = "LOGIN"
     val Emails = "emailkey"
     val Password = "passwordkey"
     val AccessToken="accesstoken"
     private val LoginSuccess ="loginSuccesskey"*/
    var SharedPreferences: SharedPreferences? = null
    val mypreferences = "LOGIN"
    private val SPLASH_DELAY: Long = 3000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.splash_screen)
//         Handler().postDelayed({
//
//             startActivity(Intent(this, LMSlogin::class.java))
//             finish()
//         }, SPLASH_DELAY)

        SharedPreferences = getSharedPreferences(mypreferences, Context.MODE_PRIVATE)

        val loginstatus: Boolean = SharedPreferences?.getBoolean("loginSuccesskey", false)!!
        println("key:$loginstatus")
        if (loginstatus.equals(true)) {
            Handler().postDelayed({

                startActivity(Intent(this, HomeScreenActivity::class.java))
                finish()
            }, SPLASH_DELAY)

        } else {
            Handler().postDelayed({

                startActivity(Intent(this, LMSLogin::class.java))
                finish()
            }, SPLASH_DELAY)
           /* editor?.clear()
            finish()*/

        }
    }
}
/* SharedPreferences = getSharedPreferences(mypreferences, Context.MODE_PRIVATE)
 val RememberMe=findViewById<CheckBox>(R.id.cb_remember_me_lgn)

 var editor = SharedPreferences?.edit()
 RememberMe.setOnCheckedChangeListener { _, isChecked ->

   if (isChecked) {

         println("remember me clicked")

     }
         editor?.putBoolean(LoginSuccess,true)
         editor?.apply()


     } else {
         editor?.putBoolean(LoginSuccess,false)
         editor?.apply()
         editor?.clear()
         Handler().postDelayed({

             startActivity(Intent(this, LMSlogin::class.java))
             finish()
         }, SPLASH_DELAY)
     }*/


/* val Email: String = SharedPreferences?.getString("emailkey", null)!!
 val Password: String = SharedPreferences?.getString("passwordkey", null)!!
 println("email:$Email")
 println("password:$Password")*/
/*cb_remember_me_lgn.setOnCheckedChangeListener { _, isChecked ->
    if (isChecked) {

    }
}*/


//}
