package com.mobileapp.lms.view.HomePage

import com.mobileapp.lms.Model.dummyMOdel.DummyMOdel

/**
 *  Module name:
 *  created: 24/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
class LogoutPresenter(private val logOutView: LogoutView,
                      private val logOutInteractor: LogoutInteractor) {


    fun logout(id:Int){


        logOutInteractor.logout(id,object :LogoutInteractor.logoutcallback{
            override fun onSuccess() {
                logOutView.onSuccess()
            }

            override fun onError(error: String) {
                logOutView.onError(error)
            }

            override fun displayLogoutProgress() {
                logOutView.displayProgress()
            }

            override fun dismissLogoutProgress() {
                logOutView.dismissProgress()
            }


        })
    }
    fun displayData(id:Int){
        logOutInteractor.displayData(id,object :LogoutInteractor.DisplayDataCallback{
            override fun onSuccess(profileList: DummyMOdel) {
                logOutView.onSuccess(profileList)

            }

            override fun onError(error: String) {
logOutView.onDisplayError(error)            }

            override fun displayProgress() {
                logOutView.displayProgress()
            }

            override fun dismissProgress() {
                logOutView.dismissProgress()
            }


        })



    }
}