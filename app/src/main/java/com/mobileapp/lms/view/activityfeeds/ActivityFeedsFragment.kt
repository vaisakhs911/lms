package com.mobileapp.lms.view.activityfeeds


import android.app.ProgressDialog
import android.content.ContentValues.TAG
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.LayoutInflater
import android.widget.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.shimmer.ShimmerFrameLayout
import com.mobileapp.lms.Model.ActivityFeeds.feedlist.FeedListModel
import com.mobileapp.lms.Presenter.Adapter.ActivityFeedsAdapter
import com.mobileapp.lms.R


/**
 * A simple [Fragment] subclass.
 */
@Suppress("UNREACHABLE_CODE", "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ActivityFeedsFragment : Fragment(), ActivityFeedsView {
    private var dialog: ProgressDialog? = null
    private var mActivitiesPresenter: ActivityFeedsPresenter? = null
    private var searchView: androidx.appcompat.widget.SearchView? = null
    private lateinit var recyclerView: RecyclerView
    private var cameraButton: RelativeLayout? = null
    private var heading: TextView? = null
    private var notificationCount: RelativeLayout? = null
    private var icNotification: ImageView? = null
    private var icEdit: ImageView? = null
    private var icSearch: ImageView? = null
    private var rLayoutSearch: RelativeLayout? = null
    var sharedPreferences: SharedPreferences? = null
    private var mShimmerViewContainer: ShimmerFrameLayout? = null
    private var rlMainLayout: LinearLayout? = null
    private val myPreferences = "LOGIN"
    private var spinner:Spinner?=null
    private var empId:Int?=null



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view: View = inflater.inflate(R.layout.fragment_activity_feeds, container, false)
        dialog = ProgressDialog(activity)
        recyclerView = view.findViewById(R.id.rv_activity_feeds)
        initView(view)






        recyclerView.layoutManager = LinearLayoutManager(activity!!.applicationContext)
        return view
    }


    private fun initView(view: View) {

        rlMainLayout = view.findViewById(R.id.main_layout_activity_feeds)
        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_activityFeeds_container)
        mActivitiesPresenter = ActivityFeedsPresenter(this, ActivityFeedsInteractor())
         spinner = view.findViewById(R.id.spinner_most_popular)
        val adapter = ArrayAdapter.createFromResource(
            context,
            R.array.priority_feeds,
            R.layout.feeds_spinner_item
        )

        cameraButton = view.findViewById(R.id.rl_camera_feeds)
        heading = activity?.findViewById(R.id.tv_heading_hme_screen)
        notificationCount = activity?.findViewById(R.id.rl_count_of_notificatns_feeds)
        icNotification = activity?.findViewById(R.id.img_notifications_active_feeds)
        icEdit = activity?.findViewById(R.id.edit_profile_icon)
        icSearch = activity?.findViewById(R.id.img_searchbtn_activityfeeds)
        rLayoutSearch = view.findViewById(R.id.rl_search_box_feeds)
        searchView = view.findViewById(R.id.searchView)
        connectionWithPresenter()
        searchViewQueryListener()
        spinner?.adapter = adapter
        adapter.setDropDownViewResource(R.layout.feeds_spinner_dropdown_item)
        adapter.notifyDataSetChanged()

        spinnerListener()
        cameraButtonClick()
        visibilityOfIcons()
    }

    private fun connectionWithPresenter() {
        sharedPreferences = context?.getSharedPreferences(myPreferences, Context.MODE_PRIVATE)
        empId= sharedPreferences?.getInt("id",0)!!
        println("Id:$id")
        mActivitiesPresenter?.activityFeeds(empId!!)
    }

    private fun cameraButtonClick() {
        cameraButton?.setOnClickListener {
            val popupView: View =
                LayoutInflater.from(this.context)
                    .inflate(R.layout.custom_popup_activity_feeds, null)

            val post: RelativeLayout = popupView.findViewById(R.id.rl_post_pop)

            val popupWindow = PopupWindow(
                popupView,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
            )


            post.setOnClickListener {
                popupWindow.dismiss()

            }
            popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0)

        }
    }

    private fun spinnerListener() {
        /*sharedPreferences = context?.getSharedPreferences(myPreferences, Context.MODE_PRIVATE)
        empId= sharedPreferences?.getInt("id",0)!!
*/

        spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                val selectedItem=spinner?.selectedItem
                println("spinner selected:$empId")
                println("spinner selected:$selectedItem")
                println("selected value${spinner?.selectedItem}")
                mActivitiesPresenter?.activityFeedsOrder(empId!!,selectedItem.toString())
            }

        }


    }


    private fun visibilityOfIcons() {
        heading?.text = "Activity Feeds"
        notificationCount?.visibility = View.VISIBLE
        icNotification?.visibility = View.VISIBLE
        icEdit?.visibility = View.GONE
        icSearch?.visibility = View.VISIBLE

        when (rLayoutSearch?.visibility) {
            View.GONE -> {
                icSearch?.setOnClickListener {

                    rLayoutSearch!!.visibility = View.VISIBLE

                }
            }
            else -> {
                rLayoutSearch?.visibility = View.VISIBLE
                icSearch?.setOnClickListener {

                    rLayoutSearch?.visibility = View.GONE

                }

            }
        }

    }


    private fun searchViewQueryListener() {

        searchView?.setOnQueryTextListener(object :
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                val query=query
                mActivitiesPresenter?.activityFeedsSearch(empId!!,query!!)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                Log.i(TAG, "Llego al querytextchange")

                return true
            }
        })
        searchView?.setOnCloseListener  (object: SearchView.OnCloseListener{
            override fun onClose(): Boolean {
                mActivitiesPresenter?.activityFeeds(empId!!)
             return false
            }


        })


    }

    override fun dismissProgress() {


        rlMainLayout?.visibility=View.VISIBLE
        mShimmerViewContainer?.stopShimmerAnimation()
        mShimmerViewContainer?.visibility = View.GONE
     /*   dialog?.dismiss()*/

    }

    override fun displayProgress() {

        rlMainLayout?.visibility=View.GONE
        mShimmerViewContainer?.startShimmerAnimation()
        mShimmerViewContainer?.visibility = View.VISIBLE
        /*dialog!!.setMessage("Loading. Please wait...")
        dialog!!.isIndeterminate = true
        dialog?.setCancelable(false)
        dialog!!.show()*/

    }

    override fun onError(error: String) {

        Toast.makeText(context, "something went wrong!!!!!!!", Toast.LENGTH_LONG).show()

    }


    override fun onSuccess(activityfeeds: FeedListModel) {

        if (activityfeeds.result != null) {
            val feedsAdapter = ActivityFeedsAdapter(activityfeeds.result, activity)
            recyclerView.adapter = feedsAdapter
            println(message = "model:$activityfeeds")
            recyclerView.isNestedScrollingEnabled = false

        }

    }



}
