package com.mobileapp.lms.view.activityfeeds

import com.mobileapp.lms.Model.ActivityFeeds.feedlist.FeedListModel

/**
 *  Module name:
 *  created: 14/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
class ActivityFeedsPresenter(
    private val activityFeedsView: ActivityFeedsView,
    private val activityFeedsInteractor: ActivityFeedsInteractor
) {
    fun activityFeeds(Id:Int) {


        activityFeedsInteractor.activityFeeds(Id,object :
            ActivityFeedsInteractor.ActivityFeedsCallback {
            override fun dismissProgress() {
                activityFeedsView.dismissProgress()
            }

            override fun displayProgress() {
                activityFeedsView.displayProgress()
            }

            override fun onSuccess(activityFeeds: FeedListModel) {
                activityFeedsView.onSuccess(activityFeeds)
            }

            override fun onError(error: String) {
                activityFeedsView.onError(error)
            }


        })
    }
    fun activityFeedsOrder(Id:Int,Order:String) {
        println("activity:$Order")

        activityFeedsInteractor.activityFeedsOrder(Id,Order,object :
            ActivityFeedsInteractor.ActivityFeedsCallback {
            override fun dismissProgress() {
                activityFeedsView.dismissProgress()
            }

            override fun displayProgress() {
                activityFeedsView.displayProgress()
            }

            override fun onSuccess(activityFeeds: FeedListModel) {
                activityFeedsView.onSuccess(activityFeeds)
            }

            override fun onError(error: String) {
                activityFeedsView.onError(error)
            }


        })
    }
    fun activityFeedsSearch(Id:Int,query:String) {


        activityFeedsInteractor.activityFeedsSearch(Id,query,object :
            ActivityFeedsInteractor.ActivityFeedsCallback {
            override fun dismissProgress() {
                activityFeedsView.dismissProgress()
            }

            override fun displayProgress() {
                activityFeedsView.displayProgress()
            }

            override fun onSuccess(activityFeeds: FeedListModel) {
                activityFeedsView.onSuccess(activityFeeds)
            }

            override fun onError(error: String) {
                activityFeedsView.onError(error)
            }


        })
    }

}