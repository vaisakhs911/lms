package com.mobileapp.lms.Model.ForgotPasswordParam

/**
 *  Module name:
 *  created: 5/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
data class ForgottPasswordParam(
    val email: String
)