package com.mobileapp.lms.Model.editProfile

/**
 *  Module name:
 *  created: 5/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
data class EditPreviewModel(




val status : Int,
val message : String,
val result : Result


)