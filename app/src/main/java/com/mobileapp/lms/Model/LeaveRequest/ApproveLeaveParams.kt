package com.mobileapp.lms.Model.LeaveRequest

/**
 *  Module name:
 *  created: 12/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
data class ApproveLeaveParams(
    val status: Int,
    var id: Int
) {
}