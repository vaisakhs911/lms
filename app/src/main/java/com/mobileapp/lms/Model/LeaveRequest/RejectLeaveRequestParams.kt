package com.mobileapp.lms.Model.LeaveRequest

/**
 *  Module name:
 *  created: 13/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
data class RejectLeaveRequestParams(
    var id:Int,
    var status:Int,
    var reason:String,
    var EmployeeId:Int

) {
}