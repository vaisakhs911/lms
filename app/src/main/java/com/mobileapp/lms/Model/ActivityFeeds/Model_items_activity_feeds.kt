package com.mobileapp.lms.Model.ActivityFeeds

/**
 *  Module name:
 *  created: 24/1/20
 *  Author: AJITH TS
 *  Revisions:
 */
class Model_items_activity_feeds {
    var creator_name: String
    var creator_duration: String
    var post: String
    var viewer_name: String
    var viewer_duration: String
    var viewer_comment: String
    var img_creator: Int = 0
    var img_viewer: Int = 0
    var img_post: Int = 0


    constructor(
        creator_name: String, creator_duration: String, post: String,
        viewer_name: String, viewer_duration: String, viewer_comment: String,
        img_creator: Int, img_viewer: Int, img_post: Int
    ) {

        this.creator_name = creator_name
        this.creator_duration = creator_duration
        this.post = post
        this.viewer_name = viewer_name
        this.viewer_duration = viewer_duration
        this.viewer_comment = viewer_comment
        this.img_creator = img_creator
        this.img_viewer = img_viewer
        this.img_post = img_post

    }
}