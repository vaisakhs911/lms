package com.example

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoginResponseModel {
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("acesstoken")
    @Expose
    var acesstoken: String? = null
    @SerializedName("result")
    @Expose
    var result: List<Result>? = null

}