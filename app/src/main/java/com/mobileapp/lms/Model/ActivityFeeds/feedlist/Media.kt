package com.mobileapp.lms.Model.ActivityFeeds.feedlist

import com.google.gson.annotations.SerializedName

class Media {
    @SerializedName("type")
    var type: String? = null
    @SerializedName("url")
    var url: String? = null

}