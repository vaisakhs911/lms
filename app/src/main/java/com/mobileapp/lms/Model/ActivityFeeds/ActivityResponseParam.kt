package com.mobileapp.lms.Model.ActivityFeeds

/**
 *  Module name:
 *  created: 12/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
data class ActivityResponseParam(

    val username : String,
    val media : String,
    val comments : String,
    val sharecount : String,
    val commentcount : String,
    val likecount : String,
    val isactive : String,
    val updatedat : String
) {
}