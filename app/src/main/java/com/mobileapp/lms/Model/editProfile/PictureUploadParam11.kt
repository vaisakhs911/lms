package com.mobileapp.lms.Model.editProfile

/**
 *  Module name:
 *  created: 6/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
data class PictureUploadParam11 (
    val fieldname : String,
    val originalname : String,
    val encoding : String,
    val mimetype : String,
    val destination : String,
    val filename : String,
    val path : String,
    val size : Int
){
}