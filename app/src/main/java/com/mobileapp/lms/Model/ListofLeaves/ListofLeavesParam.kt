package com.mobileapp.lms.Model.ListofLeaves

/**
 *  Module name:
 *  created: 31/1/20
 *  Author: AJITH TS
 *  Revisions:
 */
data class ListofLeavesParam(

    var username: String,
    var leavetype: Int,
    var leavepattern: Int,
    var from: String,
    var to: String,
    var numberofdaysrequired: Int,
    var reason: String,
    var status: Int) {
}


//        @SerializedName("id")
//        @Expose
//        private var id: Int? = null
//        @SerializedName("name")
//        @Expose
//        private var name:String? = null
//        @SerializedName("leavetype")
//        @Expose
//        private var leavetype: String? = null
//        @SerializedName("leavepattern")
//        @Expose
//        private var leavepattern: String? = null
//        @SerializedName("from")
//        @Expose
//        private var from: String? = null
//        @SerializedName("to")
//        @Expose
//        private var to: String? = null
//        @SerializedName("numberofdaysrequired")
//        @Expose
//        public var numberofdaysrequired = null
//        @SerializedName("reason")
//        @Expose
//        private var reason: String? = null
//        @SerializedName("status")
//        @Expose
//        public var status = null
//
//        fun getId(): Int? {
//            return id
//        }
//
//        fun setId(id: Int?) {
//            this.id = id
//        }
//
//        fun getName(): String? {
//            return name
//        }
//
//        fun setName(name: String?) {
//            this.name = name
//        }
//
//        fun getLeavetype(): String? {
//            return leavetype
//        }
//
//        fun setLeavetype(leavetype: String?) {
//            this.leavetype = leavetype
//        }
//
//        fun getLeavepattern(): String? {
//            return leavepattern
//        }
//
//        fun setLeavepattern(leavepattern: String?) {
//            this.leavepattern = leavepattern
//        }
//
//
//        fun getFrom(): String? {
//            return from
//        }
//
//        fun setFrom(from: String?) {
//            this.from = from
//        }
//
//        fun getTo(): String? {
//            return to
//        }
//
//        fun setTo(to: String?) {
//            this.to = to
//        }
//
//        fun getReason(): String? {
//            return reason
//        }
//
//        fun setReason(reason: String?) {
//            this.reason = reason
//        }
//
//
//        fun getNumberofdaysrequired(): Int? {
//            return numberofdaysrequired
//        }
//
//        fun setNumberofdaysrequired(numberofdaysrequired: Int?) {
//            this.numberofdaysrequired = numberofdaysrequired as Nothing?
//        }
//
//        fun status(): String? {
//            return status()
//        }
//
//        fun setStatus(status: Objects?) {
//            this.status = status as Nothing?
//        }



