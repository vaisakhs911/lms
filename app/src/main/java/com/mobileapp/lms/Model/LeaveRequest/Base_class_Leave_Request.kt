package com.mobileapp.lms.Model.LeaveRequest

/**
 *  Module name:
 *  created: 18/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
data class Base_class_Leave_Request(
    val status: Int,
    val message: String,
    val result: List<Subclass_Leave_Request>
) {
}