package com.mobileapp.lms.Model.ApplyLeaveParam

import com.google.gson.annotations.SerializedName
class ApplyleaveResponseModel {
    @SerializedName("createdby")
    var createdby: Long? = null
    @SerializedName("departmentid")
    var departmentid: Long? = null
    @SerializedName("designationid")
    var designationid: Long? = null
    @SerializedName("EmployeeId")
    var employeeId: Long? = null
    @SerializedName("from")
    var from: String? = null
    @SerializedName("id")
    var id: Int? = null
    @SerializedName("leavepattern")
    var leavepattern: Long? = null
    @SerializedName("leavetype")
    var leavetype: Long? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("numberofdaysrequired")
    var numberofdaysrequired: Long? = null
    @SerializedName("reason")
    var reason: String? = null
    @SerializedName("status")
    var status: Long? = null
    @SerializedName("to")
    var to: String? = null

}