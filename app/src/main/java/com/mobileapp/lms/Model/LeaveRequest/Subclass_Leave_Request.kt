package com.mobileapp.lms.Model.LeaveRequest

/**
 *  Module name:
 *  created: 18/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
data class Subclass_Leave_Request(
    val username: String,
    val Photo: String,
    val id:Int,
    val leavetype: String,
    val name: String,
    val leavename: String,
    val from: String,
    val to: String,
    val EmployeeId:Int,
    val numberofdaysrequired: Int,
    val reason: String,
    val status: Int,
    val designation: String,
    val departmentname: String
)

