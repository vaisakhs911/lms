package com.mobileapp.lms.Model.ProfileListParam

import android.text.Editable

/**
 *  Module name:
 *  created: 4/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
class ProfileDetailsParam(
    val id: Int,
    val FirstName: String,
    val LastName: String,
    val Email: String,
    val Password: String,
    val Salt: String,
    val EmployeeId: String,
    val DOB: String,
    val DesignationId: Int,
    val EmployeeType: String,
    val UserType: Int,
    val Phone: String,
    val AlterPhone: String,
    val BloodGroup: String,
    val Address: String,
    val Status: String,
    val AvailabilityStatus: String,
    val EmailVerified: String,
    val Photo: String,
    val ReportedTo: Int,
    val Skills: String,
    val Location: String,
    val CreatedAt: String,
    val UpdatedAt: String,
    val Blocked: String,
    val IsDeleted: String,
    val DeletedBy: String,
    val CreatedBy: String,
    val DateOfJoining: String,
    val MaritalStatus: String,
    val JobDescription: String,
    val YearOfExperience: String,
    val DepartmentId: String,
    val Pincode: String,
    val Yearofexperiance: String,
    val country: String,
val gender:String
) {

}