package com.mobileapp.lms.Model.profiledisplay

/**
 *  Module name:
 *  created: 18/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
data class ProfileDisplayModel(val status : Int,
                               val message : String,
                               val result : Result) {
}