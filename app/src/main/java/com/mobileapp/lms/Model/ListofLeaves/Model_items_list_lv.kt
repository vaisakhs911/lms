package com.mobileapp.lms.Model.ListofLeaves

/**
 *  Module name:
 *  created: 17/1/20
 *  Author: AJITH TS
 *  Revisions:
 */
public class Model_items_list_lv {
    lateinit var name: String
    lateinit var days_lve: String
    lateinit var status: String

    constructor(name:String,days_lve:String,status:String) {
        this.name=name
        this.days_lve=days_lve
        this.status=status
    }
}