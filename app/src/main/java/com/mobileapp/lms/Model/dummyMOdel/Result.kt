package com.mobileapp.lms.Model.dummyMOdel

import com.google.gson.annotations.SerializedName



class Result {
    @SerializedName("FirstName")
    var firstname: String? = null
    @SerializedName("Address")
    var address: String? = null
    @SerializedName("Address2")
    var address2: String? = null
    @SerializedName("BloodGroup")
    var bloodGroup: String? = null
    @SerializedName("country")
    var country: String? = null
    @SerializedName("DOB")
    var dOB: String? = null
    @SerializedName("DateOfJoining")
    var dateOfJoining: String? = null
    @SerializedName("Email")
    var email: String? = null
    @SerializedName("EmployeeId")
    var employeeId: String? = null
    @SerializedName("gender")
    var gender: Any? = null
    @SerializedName("Id")
    var id: Long? = null
    @SerializedName("Phone")
    var phone: String? = null
    @SerializedName("Photo")
    var photo: String? = null
    @SerializedName("Pincode")
    var pincode: String? = null
    @SerializedName("UserType")
    var userType: Long? = null
    @SerializedName("designation")
    var designation: String? = null
    @SerializedName("departmentname")
    var departmentname: String? = null

}