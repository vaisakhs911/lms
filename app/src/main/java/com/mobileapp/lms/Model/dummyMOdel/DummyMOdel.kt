package com.mobileapp.lms.Model.dummyMOdel

import com.google.gson.annotations.SerializedName



class DummyMOdel {
    @SerializedName("message")
    var message: String? = null
    @SerializedName("result")
    var result: List<Result>? = null
    @SerializedName("status")
    var status: Long? = null

}