package com.mobileapp.lms.Model.ActivityFeeds

/**
 *  Module name:
 *  created: 18/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
data class feeds_Base_class (
    val message : String,
    val status : Int,
    val sub : List<feeds_Sub_class>
) {
}