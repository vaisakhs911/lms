package com.mobileapp.lms.Model.profiledisplay

/**
 *  Module name:
 *  created: 18/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
data class Result(

    val id : Int,
    val email : String,
    val employeeId : String,
    val dOB : String,
    val userType : Int,
    val phone : String,
    val bloodGroup : String,
    val address : String,
    val photo : String,
    val dateOfJoining : String,
    val pincode : Int,
    val country : String,
    val gender : String
) {
}