package com.mobileapp.lms.Model.editProfile

import com.google.gson.annotations.SerializedName

class File {
    @SerializedName("destination")
    var destination: String? = null
    @SerializedName("encoding")
    var encoding: String? = null
    @SerializedName("fieldname")
    var fieldname: String? = null
    @SerializedName("filename")
    var filename: String? = null
    @SerializedName("mimetype")
    var mimetype: String? = null
    @SerializedName("originalname")
    var originalname: String? = null
    @SerializedName("path")
    var path: String? = null
    @SerializedName("size")
    var size: Long? = null

}