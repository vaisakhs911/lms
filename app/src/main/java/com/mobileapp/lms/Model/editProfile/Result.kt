package com.mobileapp.lms.Model.editProfile

/**
 *  Module name:
 *  created: 18/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
data class Result(

    val Id : Int,
    val Email : String,
    val EmployeeId : String,
    val DOB : String,
    val UserType : Int,
    val Phone : String,
    val BloodGroup : String,
    val Address : String,
    val Photo : String,
    val DateOfJoining : String,
    val Pincode : Int,
    val country : String,
    val gender : Any
) {
}