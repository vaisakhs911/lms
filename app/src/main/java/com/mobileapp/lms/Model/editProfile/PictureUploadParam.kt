package com.mobileapp.lms.Model.editProfile

import com.google.gson.annotations.SerializedName
class PictureUploadParam {
    @SerializedName("file")
    var file: List<File>? = null

}