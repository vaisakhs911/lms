package com.mobileapp.lms.Model.ActivityFeeds.feedlist

import com.google.gson.annotations.SerializedName
import com.mobileapp.lms.Model.ActivityFeeds.feedlist.Media

class result {
    @SerializedName("commentcount")
    var commentcount: Long? = null
    @SerializedName("comments")
    var comments: String? = null
    @SerializedName("Id")
    var id: Long? = null
    @SerializedName("isactive")
    var isactive: Any? = null
    @SerializedName("likecount")
    var likecount: Long? = null
    @SerializedName("media")
    var media: Media? = null
    @SerializedName("Photo")
    var photo: String? = null
    @SerializedName("sharecount")
    var sharecount: Long? = null
    @SerializedName("updatedat")
    var updatedat: String? = null
    @SerializedName("username")
    var username: String? = null
    @SerializedName("isLiked")
    var isLiked: Boolean? = null




}