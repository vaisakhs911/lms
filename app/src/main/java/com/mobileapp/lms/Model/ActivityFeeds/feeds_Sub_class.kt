package com.mobileapp.lms.Model.ActivityFeeds

/**
 *  Module name:
 *  created: 18/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
data class feeds_Sub_class(

    val username : String,
    val media : String,
    val id : Int,
    val comments : String,
    val sharecount : Int,
    val commentcount : Int,
    val likecount : Int,
    val isactive : Boolean,
    val updatedat : String
) {
}