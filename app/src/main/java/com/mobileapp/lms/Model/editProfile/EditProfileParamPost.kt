package com.mobileapp.lms.Model.editProfile

/**
 *  Module name:
 *  created: 5/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
data class EditProfileParamPost(

    val FirstName: String,
    val Pincode: Int,
    val Address: String,
    val Address2: String,
    val country: String,
    val Phone: String,
    val DOB: String?,
    val BloodGroup: String,
    val Photo: String?

) {
}