package com.mobileapp.lms.Model.ApplyLeaveParam


/**
 *  Module name:
 *  created: 29/1/20
 *  Author: AJITH TS
 *  Revisions:
 */
data class ApplyLeaveParam(
    var EmployeeId: Int,
    var name: String,
    var leavetype: Int,
    var leavepattern: Int,
    var from: String?,
    var to: String?,
    var numberofdaysrequired: Int,
    var reason: String,
    var status: Int,
    var createdby:Int,
    var Photo:Int,
    var designation:Int

) {
//    @SerializedName("name")
//    @Expose
//    private var name: String? = null
//    @SerializedName("leavetype")
//    @Expose
//    private var leavetype: String? = null
//    @SerializedName("leavepattern")
//    @Expose
//    private var leavepattern: String? = null
//    @SerializedName("numberofdaysrequired")
//    @Expose
//    private var numberofdaysrequired: Int? = null
//    @SerializedName("from")
//    @Expose
//    private var from: String? = null
//    @SerializedName("to")
//    @Expose
//    private var to: String? = null
//    @SerializedName("reason")
//    @Expose
//    private var reason: String? = null
//    @SerializedName("EmployeeId")
//    @Expose
//    private var employeeId: Int? = null
//
//    fun getName(): String? {
//        return name
//    }
//
//    fun setName(name: String?) {
//        this.name = name
//    }
//
//    fun getLeavetype(): String? {
//        return leavetype
//    }
//
//    fun setLeavetype(leavetype: String?) {
//        this.leavetype = leavetype
//    }
//
//    fun getLeavepattern(): String? {
//        return leavepattern
//    }
//
//    fun setLeavepattern(leavepattern: String?) {
//        this.leavepattern = leavepattern
//    }
//
//    fun getNumberofdaysrequired(): Int? {
//        return numberofdaysrequired
//    }
//
//    fun setNumberofdaysrequired(numberofdaysrequired: Int?) {
//        this.numberofdaysrequired = numberofdaysrequired
//    }
//
//    fun getFrom(): String? {
//        return from
//    }
//
//    fun setFrom(from: String?) {
//        this.from = from
//    }
//
//    fun getTo(): String? {
//        return to
//    }
//
//    fun setTo(to: String?) {
//        this.to = to
//    }
//
//    fun getReason(): String? {
//        return reason
//    }
//
//    fun setReason(reason: String?) {
//        this.reason = reason
//    }
//
//    fun getEmployeeId(): Int? {
//        return employeeId
//    }
//
//    fun setEmployeeId(employeeId: Int?) {
//        this.employeeId = employeeId
//    }
}