package com.mobileapp.lms.Model.ActivityFeeds.feedlist

import com.google.gson.annotations.SerializedName

class FeedListModel {
    @SerializedName("message")
    var message: String? = null
    @SerializedName("result")
    var result: List<result>? = null
    @SerializedName("status")
    var status: Long? = null

}