package com.example

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Result {
    @SerializedName("Id")
    @Expose
    var id: Int? = null
    @SerializedName("FirstName")
    @Expose
    var firstName: String? = null
    @SerializedName("Email")
    @Expose
    var email: String? = null
    @SerializedName("EmployeeId")
    @Expose
    var employeeId: String? = null
    @SerializedName("UserType")
    @Expose
    var userType: Int? = null

}