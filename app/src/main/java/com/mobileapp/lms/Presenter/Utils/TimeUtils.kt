package com.mobileapp.lms.Presenter.Utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 *  Module name:
 *  created: 28/1/20
 *  Author: AJITH TS
 *  Revisions:
 */
class TimeUtils {
    companion object{

        fun convertTimeToServerFormat(dateFormat: String, dateStr: String) : String? {

            val outputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            val inputFormat = SimpleDateFormat(dateFormat)
            val outputFormat = SimpleDateFormat(outputPattern)
            var date: Date? = null
            var outputTimeStr: String? = null
            try {
                date = inputFormat.parse(dateStr)
                outputTimeStr = outputFormat.format(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return outputTimeStr
        }
        fun convertServerFormatToSimple(dateFormat: String, dateString: String) : String? {

            val outputPattern = "MMM dd"
            val inputFormat = SimpleDateFormat(dateFormat)
            val outputFormat = SimpleDateFormat(outputPattern)
            var date1: Date? = null
            var outputDateStr: String? = null
            try {
                date1 = inputFormat.parse(dateString)
                outputDateStr = outputFormat.format(date1)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return outputDateStr
        }

        fun convertSrverformattoSimple1(dateFormat: String, dateString: String) : String? {

            val outputPattern = "MMM dd,yyyy"
            val inputFormat = SimpleDateFormat(dateFormat)
            val outputFormat = SimpleDateFormat(outputPattern)
            var date2: Date? = null
            var outputDateStr: String? = null
            try {
                date2 = inputFormat.parse(dateString)
                outputDateStr = outputFormat.format(date2)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return outputDateStr
        }



        fun convertServerFormatTodatpickerFormat(dateFormat: String, dateStr: String) : String? {

            val outputPattern = "dd/MM/yyyy"
            val inputFormat = SimpleDateFormat(dateFormat)
            val outputFormat = SimpleDateFormat(outputPattern)
            var date: Date? = null
            var outputTimeStr: String? = null
            try {
                date = inputFormat.parse(dateStr)
                outputTimeStr = outputFormat.format(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return outputTimeStr
        }


        fun convertSrverformattoSimple2(dateFormat: String, dateString: String) : String? {

            val outputPattern = "dd MMMM yyyy"
            val inputFormat = SimpleDateFormat(dateFormat)
            val outputFormat = SimpleDateFormat(outputPattern)
            var date2: Date? = null
            var outputDateStr: String? = null
            try {
                date2 = inputFormat.parse(dateString)
                outputDateStr = outputFormat.format(date2)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return outputDateStr
        }




    }
}
//SimpleDateFormat("yyyy-MM-dd")