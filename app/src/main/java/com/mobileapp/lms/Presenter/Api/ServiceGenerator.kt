package com.mobileapp.lms.Presenter.Api

import com.google.gson.GsonBuilder
import com.mobileapp.lms.Presenter.Utils.Constants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 *  Module name:
 *  created: 28/1/20
 *  Author: AJITH TS
 *  Revisions:
 */
object ServiceGenerator {
    fun <S> creatService(serviceClass: Class<S>): S {

        val gson = GsonBuilder().setLenient().create()

//OkHttpClient client = new OkHttpClient();
        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(120, TimeUnit.SECONDS)
            .readTimeout(120, TimeUnit.SECONDS)
            .writeTimeout(120, TimeUnit.SECONDS)
            .build()

        val retrofit = Retrofit.Builder().baseUrl(Constants.BASE_URL).client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson)).build()

        return retrofit.create(serviceClass)
    }
}