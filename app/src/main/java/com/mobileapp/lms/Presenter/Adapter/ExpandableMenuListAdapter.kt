import android.content.Context
import android.graphics.Typeface
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.ImageView
import android.widget.TextView
import com.mobileapp.lms.R
import com.mobileapp.lms.Model.side_menu_model.SideMenuModel


class ExpandableMenuListAdapter(
    private val context: Context,
    private val mListDataHeader: ArrayList<String>?,
    private var mListDataChild: HashMap<String, List<SideMenuModel>>?,
    private val mView: ExpandableListView?
) : BaseExpandableListAdapter() {
    //    private val mContext: Context
//    private val mListDataHeader:List<String>
//    private val mListDataChild: HashMap<String, List<String>>
//    var expandList: ExpandableListView
    override fun getGroupCount(): Int {
        val i = mListDataHeader?.size
        Log.d("GROUPCOUNT", i.toString())
        return mListDataHeader?.size!!
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        var childCount = 0
        if (groupPosition != 2) {
            childCount = mListDataChild?.get(mListDataHeader?.get(groupPosition))
                ?.size ?: return childCount
        }
        return childCount
    }

    override fun getGroup(groupPosition: Int): String? {
        return this.mListDataHeader?.get(groupPosition)
    }


    override fun getChild(groupPosition: Int, childPosition: Int): String? {
//        Log.d(
//            "CHILD", mListDataChild[mListDataHeader[groupPosition]]
//                ?.get(childPosition)
//        )

        val test = mListDataChild?.get(mListDataHeader?.get(groupPosition))
        println("test1  $test")
        println("test2  ${mListDataHeader?.get(groupPosition)}")
        println("test3  ${mListDataChild?.get(mListDataHeader?.get(groupPosition))}")
//        return mListDataChild?.get(mListDataHeader?.get(groupPosition))
        return test?.get(childPosition)!!.title
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(
        groupPosition: Int,
        isExpanded: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View? {
        var convertView: View? = convertView
//        val headerTitle: SideMenuModel? = getGroup(groupPosition) as SideMenuModel?
        val headerTitle = getGroup(groupPosition)

        if (convertView == null) {
            val infalInflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = infalInflater.inflate(R.layout.list_header, null)
        }
        val lblListHeader = convertView?.findViewById(R.id.submenu) as TextView
        val list_image = convertView.findViewById(R.id.iconimage) as ImageView

        lblListHeader.setTypeface(null, Typeface.BOLD)
//        lblListHeader.setText(headerTitle.getIconName())
//        headerIcon.setImageDrawable(headerTitle.getIconImg())
        lblListHeader.text = headerTitle


        when (headerTitle) {
            "Activity feeds" -> {
                list_image.setImageResource(R.drawable.ic_activity_feeds)
            }
            "My Post" -> {
                list_image.setImageResource(R.drawable.ic_mypost)
            }
            "Leave Manage" -> {
                list_image.setImageResource(R.drawable.ic_leave_management)
            }
            "settings" -> {
                list_image.setImageResource(R.drawable.ic_settings)
            }
            "Interview details" -> {
                list_image.setImageResource(R.drawable.ic_interview_details)
            }
            "feedback" -> {
                list_image.setImageResource(R.drawable.ic_feedback)
            }
            "profile" -> {
                list_image.setImageResource(R.drawable.ic_profile)
            }
            "logout" -> {
                list_image.setImageResource(R.drawable.ic_activity_feeds)
            }
        }


        if (isExpanded) {
            when (groupPosition) {

                3 -> {
                    list_image.setImageResource(R.drawable.ic_settings)
                }
                4 -> {
                    list_image.setImageResource(R.drawable.ic_interview_details_selected)
                }
                1 -> {
                    list_image.setImageResource(R.drawable.ic_mypost_selected)
                }
                6 -> {
                    list_image.setImageResource(R.drawable.ic_profile_selected)

                }
            }

        }
        if (isExpanded) {
            when (groupPosition) {

                3 -> {
                    list_image.setImageResource(R.drawable.ic_settings_selected)
                }
                4 -> {
                    list_image.setImageResource(R.drawable.ic_interview_details_selected)
                }
                1 -> {
                    list_image.setImageResource(R.drawable.ic_mypost_selected)
                }
                6 -> {
                    list_image.setImageResource(R.drawable.ic_profile_selected)

                }
            }

        }






        return convertView
    }

    override fun getChildView(
        groupPosition: Int,
        childPosition: Int,
        isLastChild: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View? {
        var convertView: View? = convertView
        val childText = getChild(groupPosition, childPosition) as String
        if (convertView == null) {
            val infalInflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = infalInflater.inflate(R.layout.list_submenu, null)
        }
        val txtListChild = convertView?.findViewById(R.id.submenu) as TextView
        val childImg = convertView.findViewById(R.id.toggle) as ImageView
        txtListChild.text = childText
        childImg.setImageResource(getChildImage(groupPosition, childPosition))




        return convertView
    }

    private fun getChildImage(groupPosition: Int, childPosition: Int): Int {

        val test = mListDataChild?.get(mListDataHeader?.get(groupPosition))
        println("test1  $test")
        println("test2  ${mListDataHeader?.get(groupPosition)}")
        println("test3  ${mListDataChild?.get(mListDataHeader?.get(groupPosition))}")
//        return mListDataChild?.get(mListDataHeader?.get(groupPosition))
        return test?.get(childPosition)!!.image
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }


}


/*//in side get group view
            mView?.setOnGroupExpandListener(object :ExpandableListView.OnGroupExpandListener{
                override fun onGroupExpand(groupPosition: Int) {
                    println( "Parent expanded position :$groupPosition")
                    when(groupPosition){
                        0->{
                            list_image.setImageResource(R.drawable.ic_activity_feeds_selected)
                        }
                    }
                }

            })*/

//        mView?.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->
//           println( "Parent position :$groupPosition")
//            println("Child position :$childPosition")
//            println( "id: $id")
//            false
//        }
//        mView?.setOnGroupClickListener { parent, v, groupPosition, id ->
//
//
//
//            false
//        }


//        lblListHeader.setOnClickListener {
//            when (headerTitle) {
//                "Activity feeds" -> {
////                    list_image.setImageResource(R.drawable.ic_activity_feeds)
//                    list_image.setImageDrawable(context.resources.getDrawable(R.drawable.drawable_activity_selector))
//                }
//                "My Post" -> {
//                    list_image.setImageResource(R.drawable.ic_mypost)
//                }
//                "Leave Manage" -> {
//                    list_image.setImageResource(R.drawable.ic_leave_management)
//                }
//
//            }
//        }
