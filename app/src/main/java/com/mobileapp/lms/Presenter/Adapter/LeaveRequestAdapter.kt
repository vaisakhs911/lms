package com.mobileapp.lms.Presenter.Adapter

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mobileapp.lms.Model.LeaveRequest.Base_class_Leave_Request
import com.mobileapp.lms.Model.LeaveRequest.Subclass_Leave_Request
import com.mobileapp.lms.R
import com.mobileapp.lms.Presenter.Utils.TimeUtils
import com.mobileapp.lms.view.leaverequest.*


/**
 *  Module name:
 *  created: 16/1/20
 *  Author: AJITH TS
 *  Revisions:
 */

class LeaveRequestAdapter(
    private var itemList_leaveRequests:
    List<Subclass_Leave_Request>,
    private var mContext: Context,
    private var mLeaveRequestPresenter: LeaveRequestPresenter? = null,
    private var sharedPreferences: SharedPreferences? = null,
    private val myPreferences: String = "LOGIN"

) :
    RecyclerView.Adapter<LeaveRequestAdapter.ItemsListHolder>(), LeaveApproveView,
    LeaveRequestView, LeaveRejectView {

    private var selectedAppliedId: Int? = null

    private var jobDescriptionPopup: TextView? = null
    private var leaveDaysCount: TextView? = null
    private var cancelButton: RelativeLayout? = null
    private var btnDismiss: ImageView? = null
    private var approveButton: RelativeLayout? = null
    private var layoutReject: RelativeLayout? = null
    private var reason: EditText? = null
    private var btnDismissAlert: ImageView? = null
    private var btnCancelAlert: RelativeLayout? = null
    private var leaveDaysCount1: TextView? = null
    private var jobDescriptionRejectAlert: TextView? = null
    private var image: String? = null

    private var userNameAlert: TextView? = null
    private var leaveDetailsRejectPop: TextView? = null


    class ItemsListHolder(item: View) : RecyclerView.ViewHolder(item) {
        var name: TextView = item.findViewById(R.id.tv_emp_name)
        var countOfLeaves: TextView = item.findViewById(R.id.tv_days)
        var profilePic: ImageView = item.findViewById(R.id.img_profile_items_lv_rqst)
        var leaveApproveButton: RelativeLayout =
            item.findViewById(R.id.rl_approve_btn_leave_request)
        var leaveRejectButton: RelativeLayout = item.findViewById(R.id.rl_reject_btn_leave_request)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemsListHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.items_leave_request, parent, false)
        return ItemsListHolder(view)

    }

    override fun getItemCount(): Int {
        return itemList_leaveRequests.size

    }

    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("SetTextI18n", "DefaultLocale")
    override fun onBindViewHolder(holder: ItemsListHolder, position: Int) {
        mLeaveRequestPresenter = LeaveRequestPresenter(this, LeaveRequesrInteractor(), this, this)
        holder.name.text = itemList_leaveRequests[position].name.toUpperCase()
        leaveCountValidation(holder, position)
        leaveRequestImage(position, holder)
        selectedAppliedId = itemList_leaveRequests[position].id
        leaveApproveButtonClick(holder, position)
        leaveRejectButtonClick(holder, position)
    }

    private fun leaveRequestImage(position: Int, holder: ItemsListHolder) {
        image = itemList_leaveRequests[position].Photo
        Glide
            .with(mContext)
            .load("https://cubets3dev.s3.us-west-1.amazonaws.com/" + image)
            .apply(RequestOptions().override(300, 300))
            .into(holder.profilePic)

    }

    private fun leaveCountValidation(
        holder: ItemsListHolder,
        position: Int
    ) {
        val dateFormatFrom = TimeUtils.convertServerFormatToSimple(
            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
            itemList_leaveRequests[position].from
        )
        val dateFormatTo = TimeUtils.convertServerFormatToSimple(
            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
            itemList_leaveRequests[position].to
        )
        val stringBuilder2 = StringBuilder()
        val days = itemList_leaveRequests[position].numberofdaysrequired
        stringBuilder2.append(days)
        if (days > 1) {
            stringBuilder2.append("DAYS")
        } else {
            stringBuilder2.append("DAY")
        }
        stringBuilder2.append("(")
        stringBuilder2.append(dateFormatFrom)
        stringBuilder2.append("-")
        stringBuilder2.append(dateFormatTo)
        stringBuilder2.append(")")
        stringBuilder2.toString()
        holder.countOfLeaves.text = stringBuilder2.toString()
    }


    private fun leaveRejectButtonClick(holder: ItemsListHolder, position: Int) {
        holder.leaveRejectButton.setOnClickListener {
            selectedAppliedId = itemList_leaveRequests[position].id
            showRejectPopup(position)
        }

    }

    private fun leaveApproveButtonClick(holder: ItemsListHolder, position: Int) {
        holder.leaveApproveButton.setOnClickListener {
            selectedAppliedId = itemList_leaveRequests[position].id
            showApprovePopup(position)
        }
    }


    private fun showApprovePopup(
        position: Int
    ) {

        val popupView: View =
            LayoutInflater.from(this.mContext)
                .inflate(R.layout.custom_pop_approve_leave, null, false)
        val popupWindow = PopupWindow(
            popupView,
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        initViewPopup(popupView, position)
        initViewMain(position)
        approveButtonClick(position)
        dismissButtonClick(popupWindow)
        cancelButtonClick(popupWindow)
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0)
    }

    @SuppressLint("DefaultLocale")
    private fun initViewPopup(popupView: View, position: Int) {
        val userNamePopup = popupView.findViewById<TextView>(R.id.tv_emp_name_approve_popup)
        jobDescriptionPopup = popupView.findViewById(R.id.tv_designation_approve_popup)
        leaveDaysCount = popupView.findViewById(R.id.tv_days_leave_approve_popup)
        val leaveDetails = popupView.findViewById<TextView>(R.id.tv_leave_type_approve_popup)
        btnDismiss = popupView.findViewById<View>(R.id.img_close_popup) as ImageView
        cancelButton = popupView.findViewById(R.id.rl_cancel_btn)
        approveButton = popupView.findViewById(R.id.rl_approve_btn)
        userNamePopup.text = itemList_leaveRequests[position].name.toUpperCase()
        val leaveType = itemList_leaveRequests[position].leavetype
        val leavePattern = itemList_leaveRequests[position].leavename
        leaveDetails.text = displayLeaveDetails(leaveType, leavePattern)
    }

    private fun displayLeaveDetails(
        leaveType: String,
        leavePattern: String
    ): String {
        val stringBuilder4 = StringBuilder()
        stringBuilder4.append(leaveType)
        stringBuilder4.append("-")
        stringBuilder4.append(leavePattern)

        return stringBuilder4.toString()
    }

    private fun initViewMain(
        position: Int
    ) {
        val designationName: String? = itemList_leaveRequests[position].designation
        val departmentName: String? = itemList_leaveRequests[position].departmentname
        if (departmentName != null || designationName != null) {
            jobDescriptionValidation(departmentName!!, designationName!!)
        }
        leaveDaysCount?.text = leaveDaysCountValidation(position)
    }

    private fun cancelButtonClick(popupWindow: PopupWindow) {
        cancelButton?.setOnClickListener {
            popupWindow.dismiss()
        }
    }

    private fun dismissButtonClick(popupWindow: PopupWindow) {
        btnDismiss?.setOnClickListener {
            popupWindow.dismiss()
        }
    }

    private fun approveButtonClick(position: Int) {
        approveButton?.setOnClickListener {
            mLeaveRequestPresenter?.leaveApprove(selectedAppliedId!!, selectedAppliedId!!)
            println("the position is changed:$position")

        }
    }


    @SuppressLint("DefaultLocale")
    private fun jobDescriptionValidation(
        departmentName: String,
        designationName: String
    ) {
        val stringBuilder5 = StringBuilder()
        if (departmentName.isNotEmpty() && designationName.isNotEmpty()) {
            stringBuilder5.append(designationName.toUpperCase())
            stringBuilder5.append("-")
            stringBuilder5.append(departmentName.toUpperCase())
            jobDescriptionPopup?.visibility = View.VISIBLE
        } else if (designationName.isNotEmpty()) {
            stringBuilder5.append(designationName.toUpperCase())
            stringBuilder5.append("-")
            jobDescriptionPopup?.visibility = View.VISIBLE
        } else if (departmentName.isNotEmpty()) {
            stringBuilder5.append(departmentName.toUpperCase())
            jobDescriptionPopup?.visibility = View.VISIBLE
        } else {
            jobDescriptionPopup?.visibility = View.GONE
        }
        jobDescriptionPopup?.text = stringBuilder5
    }

    private fun leaveDaysCountValidation(
        position: Int
    ): String {
        val fromDateFormat2 = TimeUtils.convertServerFormatToSimple(
            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
            itemList_leaveRequests[position].from
        )
        val toDateFormat2 = TimeUtils.convertServerFormatToSimple(
            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
            itemList_leaveRequests[position].to
        )
        val stringBuilder3 = StringBuilder()
        val leaveDays = itemList_leaveRequests[position].numberofdaysrequired
        stringBuilder3.append(leaveDays)
        stringBuilder3.append(" ")
        if (leaveDays > 1) {
            stringBuilder3.append("DAYS")
        } else {
            stringBuilder3.append("DAY")
        }
        stringBuilder3.append("(")
        stringBuilder3.append(fromDateFormat2)
        stringBuilder3.append("-")
        stringBuilder3.append(toDateFormat2)
        stringBuilder3.append(")")
        return stringBuilder3.toString()
    }

    @SuppressLint("DefaultLocale")
    private fun showRejectPopup(
        position: Int
    ) {
        sharedPreferences = mContext.getSharedPreferences(myPreferences, Context.MODE_PRIVATE)

        val employeeId: Int = sharedPreferences?.getInt("id", 0)!!


        val alertDialogBuilder = AlertDialog.Builder(mContext)
        val view = LayoutInflater.from(mContext).inflate(R.layout.custom_popup_reject_leave, null)
        alertDialogBuilder.setView(view)

        initViewMainReject(position)
        initViewAlert(view, position)
        val alertDialog = alertDialogBuilder.create()
        val leaveDetailsRejectPop = view.findViewById<TextView>(R.id.tv_leave_type_reject_popup)
        leaveDaysCount1 = view.findViewById(R.id.tv_days_leave_reject_popup)
        val leaveType = itemList_leaveRequests[position].leavetype
        val leavePattern = itemList_leaveRequests[position].leavename


        leaveDetailsRejectPop.text = displayLeaveDetails(leaveType, leavePattern)
        rejectButtonClick(employeeId, alertDialog,position)
        alertDismissButtonClick(alertDialog)
        alertCancelButtonClick(alertDialog)
        alertDialog.show()


    }

    private fun initViewAlert(view: View, position: Int) {

        layoutReject = view.findViewById(R.id.rl_reject_leave_btn)
        userNameAlert = view.findViewById(R.id.tv_emp_name_reject_popup)
        userNameAlert?.text = itemList_leaveRequests[position].name.toUpperCase()
        jobDescriptionRejectAlert = view.findViewById(R.id.tv_designation_reject_popup)
        btnDismissAlert = view.findViewById<View>(R.id.img_close_reject_popup) as ImageView
        btnCancelAlert = view.findViewById(R.id.rl_cancel_reject_pop_btn)
        reason = view.findViewById(R.id.et_reason_reject_leave)
    }

    private fun alertCancelButtonClick(alertDialog: AlertDialog) {
        btnCancelAlert?.setOnClickListener {
            alertDialog.dismiss()
        }

    }

    private fun alertDismissButtonClick(alertDialog: AlertDialog) {
        btnDismissAlert?.setOnClickListener {

            alertDialog.dismiss()
        }
    }

    private fun rejectButtonClick(employeeId: Int, alertDialog: AlertDialog,position: Int) {
        layoutReject?.setOnClickListener {
//            notifyItemChanged(position)
            println("the reject position is changed:$position")
            if (reason?.text.isNullOrEmpty()) {
                reason?.error = "Please enter the reason"
            } else {
                alertDialog.dismiss()
                mLeaveRequestPresenter?.leaveReject(
                    employeeId,
                    employeeId,
                    reason?.text.toString(),
                    selectedAppliedId!!
                )

//                val intent = Intent(mContext, LeaveRequest::class.java)
//                mContext.startActivity(intent)

            }

        }
    }


    private fun initViewMainReject(position: Int) {
        val designationName: String? = itemList_leaveRequests[position].designation
        val departmentName: String? = itemList_leaveRequests[position].departmentname
        if (departmentName != null || designationName != null) {
            jobDescriptionValidation1(departmentName!!, designationName!!)
        }
        leaveDaysCount1?.text = leaveDaysCountValidation(position)


    }

    @SuppressLint("DefaultLocale")
    private fun jobDescriptionValidation1(departmentName: String, designationName: String) {

        val stringBuilder5 = StringBuilder()
        if (departmentName.isNotEmpty() && designationName.isNotEmpty()) {
            stringBuilder5.append(designationName.toUpperCase())
            stringBuilder5.append("-")
            stringBuilder5.append(departmentName.toUpperCase())
            jobDescriptionRejectAlert?.visibility = View.VISIBLE
        } else if (designationName.isNotEmpty()) {
            stringBuilder5.append(designationName.toUpperCase())
            stringBuilder5.append("-")
            jobDescriptionRejectAlert?.visibility = View.VISIBLE
        } else if (departmentName.isNotEmpty()) {
            stringBuilder5.append(departmentName.toUpperCase())
            jobDescriptionRejectAlert?.visibility = View.VISIBLE
        } else {
            jobDescriptionRejectAlert?.visibility = View.GONE
        }
        jobDescriptionRejectAlert?.text = stringBuilder5
    }

    override fun onError(error: String) {
        Toast.makeText(mContext, "Something went wrong!!", Toast.LENGTH_SHORT).show()
    }

    override fun onSuccess(applyList: Base_class_Leave_Request) {

        if(applyList!=null && applyList.result!=null) {
            if (applyList.result.isNotEmpty()) {
                itemList_leaveRequests = applyList.result
                if (itemList_leaveRequests.isNotEmpty()) {
                    notifyDataSetChanged()
                }
            }
        }else{
            itemList_leaveRequests= ArrayList<Subclass_Leave_Request>()
            notifyDataSetChanged()
        }
        Toast.makeText(mContext, applyList.message, Toast.LENGTH_LONG)
            .show()
    }

    override fun dismissProgress() {
    }

    override fun displayProgress() {
    }

    override fun onApproveSuccess() {
        mLeaveRequestPresenter?.leaveRequests()
        Toast.makeText(mContext, "Leave approved successfully", Toast.LENGTH_SHORT).show()
    }

    override fun onApproveError(error: String) {
        Toast.makeText(mContext, "Something went wrong", Toast.LENGTH_SHORT).show()
    }

    override fun onRejectSuccess() {
        mLeaveRequestPresenter?.leaveRequests()
        Toast.makeText(mContext, "Leave Rejected Successfully", Toast.LENGTH_SHORT).show()
    }

    override fun onRejectError(error: String) {
        Toast.makeText(mContext, "Something went wrong", Toast.LENGTH_SHORT).show()
    }

}