package com.mobileapp.lms.Presenter.Adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.view.*
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mobileapp.lms.Model.LeaveRequest.Base_class_Leave_Request
import com.mobileapp.lms.Model.LeaveRequest.Subclass_Leave_Request
import com.mobileapp.lms.R
import com.mobileapp.lms.Presenter.Utils.TimeUtils
import com.mobileapp.lms.view.listofleaves.LeaveListInteractor
import com.mobileapp.lms.view.listofleaves.ListOfLeaves
import com.mobileapp.lms.view.listofleaves.ListOfLeavesPresenter
import com.mobileapp.lms.view.listofleaves.ListOfLeavesView
import java.util.*
import kotlin.collections.ArrayList


/**
 *  Module name:
 *  created: 17/1/20
 *  Author: AJITH TS
 *  Revisions:
 */
class ListLeaveAdapter(

    private var ItemList_List_Lv: List<Subclass_Leave_Request>,
    private var mContext: Context,
    private var statusState: String? = null,
    private var mListLeavesPresenter: ListOfLeavesPresenter? = null,
    private var sharedPreferences: SharedPreferences? = null,
    private val myPreferences: String = "LOGIN"

) :


    RecyclerView.Adapter<ListLeaveAdapter.LeaveItemsListHolder>(), ListOfLeavesView {
    private var selectedAppliedId: Int? = null
    private var btnDismiss: ImageView? = null
    private var profileImage:ImageView?=null

    class LeaveItemsListHolder(item: View) : RecyclerView.ViewHolder(item) {
        var name: TextView = item.findViewById(R.id.tv_emp_name_list_lv)
        var leaveCountAdapterItem: TextView = item.findViewById(R.id.tv_days_list_lv)
        var leaveStatus: TextView = item.findViewById(R.id.tv_leave_status_list_lv)
        var approveButton: RelativeLayout = item.findViewById(R.id.rl_approved_list_lv)
        var selectButtonText: TextView = item.findViewById(R.id.text_select_type)

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeaveItemsListHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.items_list_of_leaves, parent, false)
        return LeaveItemsListHolder(
            view
        )

    }

    override fun getItemCount(): Int {
        return ItemList_List_Lv.size

    }

    @SuppressLint("SetTextI18n", "DefaultLocale")
    override fun onBindViewHolder(holder: LeaveItemsListHolder, position: Int) {

        mListLeavesPresenter = ListOfLeavesPresenter(this, LeaveListInteractor())
        setSelectButtonText(holder, position)
        settingLeaveStatus(holder)
        approveButtonClick(holder, position)


        holder.name.text = ItemList_List_Lv[position].name.toUpperCase()


        holder.leaveCountAdapterItem.text = leaveDaysCountValidation(position)


    }

    private fun leaveDaysCountValidation(position: Int): String {

        val fromDateFormat = TimeUtils.convertServerFormatToSimple(
            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
            ItemList_List_Lv[position].from
        )
        val toDateFormat = TimeUtils.convertServerFormatToSimple(
            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
            ItemList_List_Lv[position].to
        )
        val stringBuilder1 = StringBuilder()
        val days = ItemList_List_Lv[position].numberofdaysrequired
        stringBuilder1.append(days)
        stringBuilder1.append(" ")
        if (days > 1) {
            stringBuilder1.append("DAYS")
        } else {
            stringBuilder1.append("DAY")
        }
        stringBuilder1.append("(")
        stringBuilder1.append(fromDateFormat)
        stringBuilder1.append("-")
        stringBuilder1.append(toDateFormat)
        stringBuilder1.append(")")
        return stringBuilder1.toString()
    }

    private fun approveButtonClick(holder: LeaveItemsListHolder, position: Int) {

        holder.approveButton.setOnClickListener {
            selectedAppliedId = ItemList_List_Lv[position].id
            showPopup(position)

        }

    }

    private fun settingLeaveStatus(holder: LeaveItemsListHolder) {
        val stringBuilder = StringBuilder()
        stringBuilder.append("Leave Status")
        stringBuilder.append("-")
        stringBuilder.append(statusState)
        holder.leaveStatus.text = stringBuilder
    }

    @SuppressLint("SetTextI18n")
    private fun setSelectButtonText(holder: LeaveItemsListHolder, position: Int) {
        val status = ItemList_List_Lv[position].status.toString()
        when (status) {
            "0" -> {
                holder.selectButtonText.text = "pending"
            }
            "1" -> {
                holder.selectButtonText.text = "Approved"
            }
            "2" -> {
                holder.selectButtonText.text = "Rejected"
            }

        }
        fetchStatus(status.toInt())
    }

    private fun fetchStatus(status: Int) {
        when (status) {
            0 -> statusState = "pending"
            1 -> statusState = "Approved"
            2 -> statusState = "Rejected"
        }
    }

    private fun showPopup(
        position: Int
    ) {


        val status = ItemList_List_Lv[position].status.toString()


        fetchStatus(status.toInt())
        val popupView: View =
            LayoutInflater.from(this.mContext).inflate(R.layout.custom_popup_approve, null)

        val popupWindow = PopupWindow(
            popupView,
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT

        )

        initViewPopup(popupView, position)
        dismissButtonClick(popupWindow)
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0)
    }

    @SuppressLint("DefaultLocale")
    private fun initViewPopup(popupView: View, position: Int) {

        profileImage=popupView.findViewById(R.id.img_profile_items_lst_lv)
        val userNamePopup = popupView.findViewById<TextView>(R.id.tv_emp_name_leavelist_popup)
        val leaveDaysCount = popupView.findViewById<TextView>(R.id.tv_days_leavelist_popup)
        val leaveDetails = popupView.findViewById<TextView>(R.id.tv_leave_type_leavelist_popup)
        val leaveReason = popupView.findViewById<TextView>(R.id.tv_leave_reason_popup)
        val leaveStatus = popupView.findViewById<TextView>(R.id.tv_leave_status_popup)
        val statusSpinner = popupView.findViewById<Spinner>(R.id.tv_select_status)
        userNamePopup.text = ItemList_List_Lv[position].name.toUpperCase()
        leaveReason.text = ItemList_List_Lv[position].reason
        btnDismiss = popupView.findViewById(R.id.image_close_popup)


        leaveStatus.text = settingLeaveStatusPopup()
        spinnerListener(statusSpinner)
        leaveDaysCount.text = leaveDaysCountValidation(position)
        leaveDetails.text = settingLeaveDetails(position)
        imageInsideListOfLeaves(position)

        val adapter = ArrayAdapter.createFromResource(
            mContext,
            R.array.Approve,
            R.layout.simple_spinner_leave_list
        )

        statusSpinner.adapter = adapter
        adapter.setDropDownViewResource(R.layout.simple_dropdown_spinner_leave_list)


    }

    private fun imageInsideListOfLeaves(position: Int) {
        val image=ItemList_List_Lv[position].Photo
        val into = Glide
            .with(mContext)
            .load("https://cubets3dev.s3.us-west-1.amazonaws.com/" + image)
            .apply(RequestOptions().override(300, 300))
            .into(profileImage!!)
    }

    private fun settingLeaveDetails(position: Int): String {
        val stringBuilder4 = StringBuilder()
        stringBuilder4.append(ItemList_List_Lv[position].leavetype)
        stringBuilder4.append("-")
        stringBuilder4.append(ItemList_List_Lv[position].leavename)
        return stringBuilder4.toString()
    }

    private fun dismissButtonClick(popupWindow: PopupWindow) {
        btnDismiss?.setOnClickListener { popupWindow.dismiss() }
    }

    private fun settingLeaveStatusPopup(): StringBuilder {
        val stringBuilder = StringBuilder()
        stringBuilder.append("Leave Status")
        stringBuilder.append("-")
        stringBuilder.append(statusState)
        return stringBuilder
    }


    private fun spinnerListener(statusSpinner: Spinner) {

        sharedPreferences = mContext.getSharedPreferences(myPreferences, Context.MODE_PRIVATE)
        val employeeId: Int = sharedPreferences?.getInt("id", 0)!!
        statusSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (statusSpinner.selectedItem?.equals("Approved")!!) {
                    notifyItemChanged(position)
                    println("selected item is most approved")
                    mListLeavesPresenter?.leaveApprovePost(selectedAppliedId!!, selectedAppliedId!!)
                    val intent = Intent(mContext, ListOfLeaves::class.java)
                    mContext.startActivity(intent)
                    (mContext as Activity).finish()
                    println("Appliedidapprove:$selectedAppliedId")
                } else if (statusSpinner.selectedItem?.equals("Rejected")!!) {
                    notifyItemChanged(position)

                    mListLeavesPresenter?.leaveRejectPost(
                        employeeId,
                        employeeId,
                        selectedAppliedId!!
                    )
                    val intent = Intent(mContext, ListOfLeaves::class.java)
                    mContext.startActivity(intent)
                    (mContext as Activity).finish()
                    println("Appliedidreject:$selectedAppliedId")
                }
                println("selected value${statusSpinner.selectedItem}")
            }

        }
    }

    override fun onError(error: String) {

    }

    override fun displayProgress() {
    }

    override fun dismissProgress() {
    }

    override fun onLeaveListedSuccess(applyList: Base_class_Leave_Request) {
        var list: List<Subclass_Leave_Request> = ItemList_List_Lv
        Collections.reverse(list)
        if(applyList!=null && applyList.result!=null) {
            if (applyList.result.isNotEmpty()) {
                list = applyList.result
                if (list.isNotEmpty()) {

                    notifyDataSetChanged()
                }
            }
        }else{
            list= ArrayList<Subclass_Leave_Request>()
            notifyDataSetChanged()
        }
    }

    override fun LeaveApprovedSuccess() {
        mListLeavesPresenter?.listOfLeavesGet()
        Toast.makeText(mContext, "Lave approved successfully", Toast.LENGTH_SHORT).show()

    }

    override fun LeaveApprovedError(error: String) {
        Toast.makeText(mContext, "something went wrong", Toast.LENGTH_SHORT).show()
    }

    override fun LeaveRejectedSuccess() {
        mListLeavesPresenter?.listOfLeavesGet()
        Toast.makeText(mContext, "Lave Rejected successfully", Toast.LENGTH_SHORT).show()
    }

    override fun LeaveRejectedError(error: String) {
        Toast.makeText(mContext, "something went wrong", Toast.LENGTH_SHORT).show()
    }




}


