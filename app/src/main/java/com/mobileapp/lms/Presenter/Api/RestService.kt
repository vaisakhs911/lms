package com.mobileapp.lms.Presenter.Api


import com.example.LoginRequestModel
import com.example.LoginResponseModel
import com.mobileapp.lms.Model.ActivityFeeds.ActivityResponseParam
import com.mobileapp.lms.Model.ActivityFeeds.feedlist.FeedListModel
import com.mobileapp.lms.Model.ApplyLeaveParam.ApplyLeaveParam
import com.mobileapp.lms.Model.ApplyLeaveParam.ApplyleaveResponseModel
import com.mobileapp.lms.Model.ForgotPasswordParam.ForgottPasswordParam
import com.mobileapp.lms.Model.LeaveRequest.ApproveLeaveParams
import com.mobileapp.lms.Model.LeaveRequest.Base_class_Leave_Request
import com.mobileapp.lms.Model.LeaveRequest.RejectLeaveRequestParams

import com.mobileapp.lms.Model.dummyMOdel.DummyMOdel
import com.mobileapp.lms.Model.editProfile.EditProfileParamPost
import com.mobileapp.lms.Model.logout.LogoutResponseModel
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 *  Module name:
 *  created: 28/1/20
 *  Author: AJITH TS
 *  Revisions:
 */
interface RestService {
    @POST("leavemanagement/create")
    fun postApplyParamsToApi(@Body applyLeaveParam: ApplyLeaveParam): Call<ApplyleaveResponseModel>

    @GET("/api/leavemanagement")
    fun listOfRequests(@Query("status")status:Int): Call <Base_class_Leave_Request>

    @POST("/api/leavemanagement/approveleave/{id}")
    fun approveleave(@Path("id") id: Int,
                     @Body leaveapproveparam: ApproveLeaveParams): Call<ResponseBody>

    @POST("/api/leavemanagement/rejectleave/{id}")
    fun rejectleave(@Path("id")id:Int,
                    @Body rejectLeaveRequestParams: RejectLeaveRequestParams):Call<ResponseBody>
    @GET("/api/leavemanagement")
    fun listOfLeaves(): Call<Base_class_Leave_Request>

    @POST("/api/auth/forgotpassword")
    fun forgotpassword(@Body forgotpasswordparam: ForgottPasswordParam): Call<ResponseBody>

    @GET("/api/profile/{id}")
    fun profile(@Path("id") id: String): Call<DummyMOdel>


    @GET("/api/profile/{id}")
    fun getDataFromEditPreviewApi(@Path("id") id: String): Call<DummyMOdel>


    @PATCH("/api/profile/{id}")
    fun editprofile(
        @Path("id") id: String,
        @Body editProfileParamPost: EditProfileParamPost
    ): Call<ResponseBody>


    @Multipart
    @POST("employee/selectfile")
    fun uploadImageFile(@Part body: MultipartBody.Part,@Part("files")name: RequestBody): Call<ResponseBody>

    @GET("activityfeed/Activepost")
    fun activityFeedsOrder(
        @Query("employeeid") employeeid:Number,
        @Query("order") order: String
    ): Call<FeedListModel>
    @GET("activityfeed/Activepost")
    fun activityFeedsSearch(
        @Query("employeeid") employeeid:Number,
        @Query("search") search: String
    ): Call<FeedListModel>
    @GET("activityfeed/Activepost")
    fun activityFeedsDisplay(@Query("employeeid")employeeid:Number): Call<FeedListModel>

    @POST("/api/auth/signin")
    fun login(@Body loginrequestmodel: LoginRequestModel):Call<LoginResponseModel>

    @POST("/api/auth/sign-out")
    fun logout(@Body logoutrequest: LogoutResponseModel):Call<ResponseBody>
}


//    @Multipart
//    @POST("/api/employee/selectpic")
//    Call<Res>
//fun uploadImage(@Part MultipartBody.Part file,
//    @Part("name") RequestBody requestBody):Call<ResponseBody>