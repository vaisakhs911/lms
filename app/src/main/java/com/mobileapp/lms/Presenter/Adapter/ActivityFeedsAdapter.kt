package com.mobileapp.lms.Presenter.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mobileapp.lms.Model.ActivityFeeds.feedlist.result
import com.mobileapp.lms.R
import com.mobileapp.lms.view.activityfeeds.DocumentActivity
import com.mobileapp.lms.view.activityfeeds.VideoViewActivity
import org.ocpsoft.prettytime.PrettyTime
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


/**
 *  Module name:
 *  created: 24/1/20
 *  Author: AJITH TS
 *  Revisions:
 */
class ActivityFeedsAdapter(
    private var ItemList_Activityfeeds: List<result>?,
    private var mContext: Context?                                   //private var searcharray:  ArrayList<feeds_Sub_class>
) : RecyclerView.Adapter<ActivityFeedsAdapter.ActivityfeedsHolder>() {                                  //Filterable
    private var image: String? = null
    private var type: String? = null
    private var creatorImage: String? = null

//    internal var mfilter: NewFilter


    class ActivityfeedsHolder(item: View) : RecyclerView.ViewHolder(item) {


        var feedCreatorName: TextView = item.findViewById(R.id.tv_post_creator_name_feeds)
        var creator_duration: TextView = item.findViewById(R.id.tv_post_creator_duration_feeds)
        var feedDescription: TextView = item.findViewById(R.id.tv_post_insights_feeds)
        var feedViewerName: TextView = item.findViewById(R.id.tv_post_viewer_name_feeds)
        var feedViewerDuration: TextView = item.findViewById(R.id.tv_post_viewer_duration_feeds)
        var feedViewerComment: TextView = item.findViewById(R.id.tv_comment_of_viewer_feeds)
        var feedCreatorImage: ImageView = item.findViewById(R.id.img_post_creator_feeds)
        var feedImage: ImageView = item.findViewById(R.id.img_post_image_feeds)
        var feedViewerImage: ImageView = item.findViewById(R.id.img_post_viewer_feeds)
        var shareCount: TextView = item.findViewById(R.id.tv_count_of_shares_feed)
        var commentCount: TextView = item.findViewById(R.id.tv_count_of_shares_feed)
        var likeCount: TextView = item.findViewById(R.id.tv_count_of_likes_feed)
        var layoutLikeShareComment: RelativeLayout =
            item.findViewById(R.id.rl_like_comment_share_feeds)
        var likedStatus: ImageView = item.findViewById(R.id.img_like_click_feeds)
        var defaultLike: ImageView = item.findViewById(R.id.img_like_feeds)
        var documentBackground = item.findViewById(R.id.document_background) as RelativeLayout
        var documentClick:ImageView=item.findViewById(R.id.img_document_open)
        var playbackBackground: RelativeLayout = item.findViewById(R.id.rlayout_post_video_feeds)
        var mediaplayblack: ImageView = item.findViewById(R.id.img_media_play)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ActivityfeedsHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.items_activity_feeds, parent, false)
        return ActivityfeedsHolder(view)


    }


    override fun getItemCount(): Int {

        return ItemList_Activityfeeds?.size!!

    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onBindViewHolder(
        holder: ActivityfeedsHolder,
        position: Int
    ) {
        image = ItemList_Activityfeeds?.get(position)?.media?.url
        type = ItemList_Activityfeeds?.get(position)?.media?.type
        println("types:$type")
        println("types:$image")
        creatorImage = ItemList_Activityfeeds?.get(position)?.photo
        println("creator image:$creatorImage")

        type = ItemList_Activityfeeds?.get(position)?.media?.type
        println("postImage:$image")
        holder.feedCreatorName.text = ItemList_Activityfeeds?.get(position)?.username
        holder.feedDescription.text = ItemList_Activityfeeds?.get(position)?.comments
        holder.shareCount.text = ItemList_Activityfeeds?.get(position)?.sharecount.toString()
        holder.commentCount.text = ItemList_Activityfeeds?.get(position)?.commentcount.toString()
        holder.likeCount.text = ItemList_Activityfeeds?.get(position)?.likecount.toString()
        println("IMAGE:$image")

        var likeStatus = ItemList_Activityfeeds?.get(position)?.isLiked
        println("likeclick:$likeStatus")

        when (likeStatus) {
            true -> {
                holder.defaultLike.visibility = View.GONE
                holder.likedStatus.visibility = View.VISIBLE


            }
            false -> {
                holder.defaultLike.visibility = View.VISIBLE
                holder.likedStatus.visibility = View.GONE

            }

        }

        findTimeAgo(position, holder)
        creatorImageValidation(holder)
        if (type == "image" || type == "photo" && image != "") {


            holder.feedImage.visibility = View.VISIBLE
            holder.playbackBackground.visibility = View.GONE
            holder.mediaplayblack.visibility = View.GONE
            holder.documentClick.visibility = View.GONE
            holder.documentBackground.visibility = View.GONE
            Glide
                .with(mContext!!)
                .load("https://cubets3dev.s3.us-west-1.amazonaws.com/$image")

                .into(holder.feedImage)
        } else if (type == "video" && image != "") {
            val videoPath=image
            holder.feedImage.visibility = View.GONE
            holder.playbackBackground.visibility = View.VISIBLE
            holder.mediaplayblack.visibility = View.VISIBLE
            holder.documentClick.visibility = View.GONE
            holder.documentBackground.visibility = View.GONE
            holder.mediaplayblack.setOnClickListener {
                image = ItemList_Activityfeeds?.get(position)?.media?.url

                mContext?.startActivity(Intent(mContext, VideoViewActivity::class.java))
                if (image !== "") {
                    val intent = Intent(mContext, VideoViewActivity::class.java)
                    intent.putExtra("videoPath", videoPath)
                    mContext?.startActivity(intent)

                }



            }

        } else if (type == "document" || type == "file" && image != "") {
            val documentPath=image
            println("DOc:$image")
            holder.feedImage.visibility = View.GONE
            holder.playbackBackground.visibility = View.GONE
            holder.mediaplayblack.visibility = View.GONE
            holder.documentClick.visibility = View.VISIBLE
            holder.documentBackground.visibility = View.VISIBLE
            holder.documentClick.setOnClickListener {

                mContext?.startActivity(Intent(mContext, DocumentActivity::class.java))
                val intent = Intent(mContext, DocumentActivity::class.java)
                println("DocumentPath:$documentPath")
                intent.putExtra("documentPath", documentPath)
                mContext?.startActivity(intent)
            }
        }


    }

    private fun findTimeAgo(position: Int, holder: ActivityfeedsHolder) {
        val timeAgo = ItemList_Activityfeeds?.get(position)?.updatedat

        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        sdf.timeZone = TimeZone.getTimeZone("GMT")
        try {
            val time: Long = sdf.parse(timeAgo).time
            val now = System.currentTimeMillis()
            val ago: CharSequence
            val prettyTime = PrettyTime(Locale.getDefault())
            ago = prettyTime.format(Date(time))
            println("timeago:$ago")
            holder.creator_duration.text = ago
        } catch (e: ParseException) {
            e.printStackTrace()

        }
    }

    private fun creatorImageValidation(holder: ActivityfeedsHolder) {
        if (creatorImage.isNullOrEmpty()) {
            holder.feedCreatorImage.visibility = View.GONE
        } else {

            Glide
                .with(mContext!!)
                .load("https://cubets3dev.s3.us-west-1.amazonaws.com/$creatorImage")
                .apply(RequestOptions.circleCropTransform())
                .apply(RequestOptions().override(150, 150))
                .into(holder.feedCreatorImage)
            println("creatorImage:$creatorImage")
        }
    }


    /*private fun postImageValidation(holder: ActivityfeedsHolder) {
        if (image.isNullOrBlank()){
            holder.layoutLikeShareComment.visibility=View.GONE


        }else{
            Glide
                .with(mContext!!)
                .load("https://cubets3dev.s3.us-west-1.amazonaws.com/$image")

                .into(holder.feedImage)


        }
    }*/


}





