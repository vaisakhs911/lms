package com.mobileapp.lms.Presenter.Api

/**
 *  Module name:
 *  created: 28/1/20
 *  Author: AJITH TS
 *  Revisions:
 */
object RestServiceBuilder {

    private var service: RestService? = null
    val apiService: RestService?
        get() {
            service =
                ServiceGenerator.creatService(RestService::class.java)
            return service
        }

}