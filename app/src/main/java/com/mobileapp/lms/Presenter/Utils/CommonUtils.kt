package com.mobileapp.lms.Presenter.Utils

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager


/**
 *  Module name:
 *  created: 17/2/20
 *  Author: AJITH TS
 *  Revisions:
 */
class CommonUtils {

    companion object {
        private var leaveType: Int? = null
        private var leavepattern: Int? = null
        fun HideFragmentKeyBoard(context: Context, view: View) {
            val inputMethodManager = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }


        public fun hideKeyBoard(activity: Activity) {

            val imm: InputMethodManager =
                activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            //Find the currently focused view, so we can grab the correct window token from it.
            //Find the currently focused view, so we can grab the correct window token from it.
            var view: View? = activity.getCurrentFocus()
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = View(activity)
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0)

        }

        public fun fetchSelectedLeaveType(selectedItem: String) {

            when {
                selectedItem.equals("Select Type") -> {
                    leaveType = 0
                }
                selectedItem.equals("Casual Leave") -> {
                    leaveType = 1
                }
                selectedItem.equals("Sick Leave") -> {
                    leaveType = 2
                }
                selectedItem.equals("Maternity Leave") -> {
                    leaveType = 3
                }
                selectedItem.equals("Paternity Leave") -> {
                    leaveType = 4
                }
            }


        }

        public fun fetchSelectedLeavePattern1(selectedItem: String) {
            when {
                selectedItem.equals("Day Type") -> {
                    leavepattern = 5
                }
                selectedItem.equals("Full Day") -> {
                    leavepattern = 6
                }
                selectedItem.equals("Half Day") -> {
                    leavepattern = 7
                }

            }
        }
    }
}